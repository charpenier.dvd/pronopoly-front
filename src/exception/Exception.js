class ApiException {

	constructor(message) {
		this.message = message;
	}
}

class AuthenticationException {

	constructor(message) {
		this.message = message;
	}
}

class ValidationException {
	constructor(message) {
		this.message = message;
	}
}

export {
	AuthenticationException,
	ApiException,
	ValidationException
};