import AppView from '../views/AppView';
import {Container} from 'flux/utils';
import LoginStore from '../data/login/LoginStore';
import LoginAction from '../data/login/LoginAction'

function getStores() {
  return [
    LoginStore,
  ];
}

function getState() {
  return {
    l: LoginStore.getState(),
    
    loginAction : LoginAction.login
  };
}

export default Container.createFunctional(AppView, getStores, getState);