import React, { Component } from 'react';
import './App.css';

import RouterProno from './data/router/RouterComponent';
import HttpsRedirect from 'react-https-redirect';

class App extends Component {
  render() {
    return (
      <div>
        <HttpsRedirect>
          <RouterProno />
        </HttpsRedirect>
      </div>
    );
  }
}

export default App;
