function tradGroup(group, letter = "") {
	switch (group) {
		case "Group":
			return `Groupe ${letter}`;
		case "Round of 16":
			return "Huitème";
		case "Quarter-finals":
			return "Quart";
		case "Semi-finals":
			return "Demie finale";
		case "Third place":
			return "Troisième place";
		case "Final":
			return "Finale";
		default:
			return "";
	}
}

function tradGroupDetail(group) {
	switch (group) {
		case "Group":
			return "Match de poule";
		case "Round of 16":
			return "Match de Huitème";
		case "Quarter-finals":
			return "Match de quart";
		case "Semi-finals":
			return "Demie finale";
		case "Third place":
			return "Match de la Troisième place";
		case "Final":
			return "Finale";
		default:
			return "";
	}
}

function isTeamWinner(matchType, team1Score, team2Score, team1Penalty, team2Penalty) {
	if (matchType === "Group" && team1Score > team2Score) {
		return "font-weight-bold";
	} else {
		const diff = team1Score - team2Score;
		if(diff === 0 && team1Penalty > team2Penalty){return "font-weight-bold"; }
		if(diff > 0){return "font-weight-bold"; }
		
	}
	return "";
}

function pronoTeamWinner(matchType, team1Score, team2Score, currentTeam, teamWinner){
	if (matchType === "Group" && team1Score > team2Score) {
		return "font-weight-bold";
	} else {
		const diff = team1Score - team2Score;
		if(currentTeam === teamWinner){return "font-weight-bold"; }
		if(diff > 0){return "font-weight-bold"; }
		
	}
	return "";
}

function getFeedClass(topic) {
	switch (topic.topic_type) {
		case "FEED":
			return "bg-light";
		case "ANNOUNCE_MATCH":
			return "text-white bg-secondary";
		case "WARNING":
			return "text-white bg-warning";
		case "ANNOUNCE":
			return "text-white bg-primary";
		default:
			return "bg-light";
	}
}

function getLinkColor(topic) {
	switch (topic.topic_type) {
		case "ANNOUNCE_MATCH":
			return "text-white";
		case "WARNING":
			return "text-white";
		case "ANNOUNCE":
			return "text-white";
		default:
			return "";
	}
}

export {
	tradGroup, tradGroupDetail, isTeamWinner, getFeedClass, getLinkColor, pronoTeamWinner
};