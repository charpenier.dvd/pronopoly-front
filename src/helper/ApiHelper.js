import {
	ApiException,
	AuthenticationException,
	ValidationException
} from '../exception/Exception';
import {
	apiURL
} from './Config';

class ApiHelper {

	constructor() {
		this.defaultUrl = apiURL;
	}


	async execute(route, method, params = null, headers = null) {
		if (!headers) {
			headers = new Headers({
				"Content-Type": "application/json"
			});
		}
		const response = await fetch(`${this.defaultUrl}${route}`, {
			method: method,
			headers: headers,
			body: params
		});
		switch (response.status) {
			case 200:
			case 201:
				return await response.json();
			case 401:
				throw new AuthenticationException('Unauthorize user');
			case 409:
				const message = await response.json();
				throw new ValidationException(message.validationMessage);
			default:
				const errorMessage = await response.json();
				throw new ApiException(errorMessage.error);
		}
	}

	async executeConnected(route, method, params = null) {
		const token = localStorage.getItem('token');
		const headers = new Headers({
			"Content-Type": "application/json",
			"x-access-token": token
		});
		return this.execute(route, method, params, headers);
	}
}

export default ApiHelper;