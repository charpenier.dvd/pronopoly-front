let apiURL = process.env.NODE_ENV === "production" ? 'https://pronopoly-backend.herokuapp.com/' : 'http://localhost:3200/';
let domain = process.env.NODE_ENV === "production" ? 'https://pronopoly.herokuapp.com' : 'http://localhost:3000';

export {
	apiURL,
	domain
}