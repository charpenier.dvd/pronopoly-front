import { Component } from 'react';

class FluxComponent extends Component{

	constructor(props, store){
		super(props);
		this.store = store;
		this.state = store.getState();
		this.listenerId = null;
	}

	storeChange = () => {
		this.setState(this.store.getState());
	}
	componentDidMount() {
		this.listenerId  = this.store.addListener(this.storeChange);
	}
	componentWillUnmount() {
		this.listenerId.remove();
	}
}

export default FluxComponent;
