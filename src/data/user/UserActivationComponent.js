import React from 'react';
import { Link } from 'react-router-dom';
import UserActivationStore from './UserActivationStore';
import FluxComponent from '../../helper/FluxComponent';
import UserFormAction from './UserFormAction';

class UserActivation extends FluxComponent {
	constructor(props) {
		super(props, UserActivationStore);

		const params = this.props.location.search.split('?token=');
		this.token = null;
		if (params && params.length > 0) {
			this.token = params[1];
		}
	}

	async componentDidMount() {
		super.componentDidMount();
		await UserFormAction.activation(this.token);
	}

	render() {
		return (
			<div className="container">
				<div className="row justify-content-around mt-4 bg-opacity">
					<div className="col-md-4">
						<div className="card">
							<div className="card-body">
								<h2>Activation de compte</h2>
								{
									this.state.success ? 'Votre compte est activé, vous pouvez vous connecter' :
										this.state.errorMessage
								}
								<br />
								<hr />
								<Link className="btn btn-primary btn-sm mr-1 mb-1" role="button" to='/'>Connexion</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default UserActivation;