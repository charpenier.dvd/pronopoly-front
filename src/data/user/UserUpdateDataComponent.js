import React from 'react';
import UserFormAction from './UserFormAction';
import UserUpdateStore from './UserUpdateStore';
import FluxComponent from '../../helper/FluxComponent';

class UserUpdateData extends FluxComponent {

	constructor(props) {
		super(props, UserUpdateStore);
	}

	componentDidMount() {
		super.componentDidMount();
		UserFormAction.initUpdateForm();
	}

	handleInputChange = (e) => {
		UserFormAction.handleInputChangeForUpdateData(e);
	}

	handleSubmit = (e) => {
		e.preventDefault();

		let isValid = true;

		const forms = document.getElementsByClassName('form1');
		Array.from(forms).forEach(function (form) {
			if (form.checkValidity() === false) {
				isValid = false;
			}
			form.classList.add('was-validated');
		});

		if (isValid) {
			UserFormAction.updateData(this.state.firstName,
				this.state.lastName,
				this.state.email,
				this.state.photoUrl);
		}
	}

	getImgStyle() {
		return {
			width: '30px',
			height: '30px',
			marginBottom: '5px'
		};
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit} className="needs-validation form1" noValidate>
				<h3>Modifier mes informations</h3>
				<div className="form-row">
					<div className="form-group col-md-6">
						<label htmlFor="firstName">Prénom*</label>
						<input type="text" id="firstName" name="firstName"
							className="form-control" required
							value={this.state.firstName} onChange={this.handleInputChange} />
						<div className="invalid-feedback">
							Veuillez saisir votre prénom
        								</div>
					</div>
					<div className="form-group col-md-6">
						<label htmlFor="lastName">Nom*</label>
						<input type="text" name="lastName" id="lastName"
							className="form-control" required
							value={this.state.lastName} onChange={this.handleInputChange} />
						<div className="invalid-feedback">
							Veuillez saisir votre nom
        								</div>
					</div>
					<div className="form-group col-md-10">
						<label htmlFor="photo">Photo url</label>
						<input type="url" name="photoUrl" id="photo"
							className="form-control" pattern="(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)"
							value={this.state.photoUrl} onChange={this.handleInputChange} />
						<div className="invalid-feedback">
							Veuillez saisir une url d'image valide
									</div>
					</div>

					<div className="form-group col-md-2 d-flex align-items-end">
						<img src={this.state.photoUrl ? this.state.photoUrl : "/img/user.png"} className="rounded-circle mr-2"
							style={this.getImgStyle()} alt="" />
					</div>
				</div>

				<button type="submit" className="btn btn-primary mb-2">Modifier mes informations</button>
				{this.state.success ?
					<div className="alert alert-primary" role="alert">
						Vos informations sont à jour.
									</div>
					: ''}
				{this.state.errorMessage ?
					<div className="alert alert-danger" role="alert">
						{this.state.errorMessage}
					</div>
					: ''
				}
			</form>
		);
	}
}

export default UserUpdateData;