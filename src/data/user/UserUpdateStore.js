import {
	ReduceStore
} from 'flux/utils';
import {
	UserActionTypes,
	LoginActionTypes
} from '../ActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';

class UserUpdateStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			firstName: '',
			lastName: '',
			photoUrl: '',
			password: '',
			email: '',
			confirmPassword: '',
			oldPassword: '',
			success: '',
			errorMessage: '',
			successPassword: '',
			errorMessagePassword: ''
		}
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case UserActionTypes.USER_UPDATE_FORM_UPDATE:
				state[action.name] = action.value;
				state.errorMessage = "";
				state.success = "";
				state.successPassword = "";
				state.errorMessagePassword = "";
				this.__emitChange();
				break;
			case UserActionTypes.INIT_UPDATE_FORM:
				state.firstName = action.user.first_name;
				state.lastName = action.user.last_name;
				state.photoUrl = action.user.photo_url;
				state.email = action.user.email_adress;
				state.errorMessage = "";
				state.success = "";
				state.successPassword = "";
				state.errorMessagePassword = "";
				this.__emitChange();
				break;
			case UserActionTypes.UPDATE_FORM_DATA_SUCCESS:
				state.success = action.message;
				this.__emitChange();
				break;
			case UserActionTypes.UPDATE_FORM_DATA_ERROR:
				state.errorMessage = action.message;
				this.__emitChange();
				break;
			case UserActionTypes.UPDATE_FORM_PASSWORD_SUCCESS:
				state.successPassword = action.message;
				this.__emitChange();
				break;
			case UserActionTypes.UPDATE_FORM_PASSWORD_ERROR:
				state.errorMessagePassword = action.message;
				this.__emitChange();
				break;
			case LoginActionTypes.DISCONNECT:
				state = this.getInitialState();
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new UserUpdateStore();