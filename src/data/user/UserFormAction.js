import {
	UserActionTypes
} from '../ActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';
import UserService from './UserAPI';
import UserAPI from './UserAPI';
import {
	ValidationException
} from '../../exception/Exception';


const UserFormActions = {

	async register(user) {
		try {
			const result = await UserService.register(user);
			PronopolyDispatcher.dispatch({
				type: UserActionTypes.USER_CREATED,
				success: result.success
			});
		} catch (err) {
			if(err instanceof ValidationException){
				PronopolyDispatcher.dispatch({
					type: UserActionTypes.USER_NOT_CREATED,
					message: err.message
				});
			} else {
				PronopolyDispatcher.dispatch({
					type: UserActionTypes.USER_NOT_CREATED,
					message: 'Erreur de création du compte'
				});
			}	
		}
	},
	async activation(token) {
		try {
			const result = await UserService.activate(token);
			PronopolyDispatcher.dispatch({
				type: UserActionTypes.USER_ACTIVATED,
				success: result.success
			});
		} catch (err) {
			PronopolyDispatcher.dispatch({
				type: UserActionTypes.USER_NOT_ACTIVATED,
				message: 'Erreur lors de l\'activation du compte, veuillez contacter l\'administrateur'
			});
		}
	},
	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		if (name === 'password' || name === 'confirmPassword') {
			const password = document.getElementById('password');
			const confifmPassword = document.getElementById('confirmPassword');
			password.setCustomValidity('');
			confifmPassword.setCustomValidity('');

			if (password.value !== confifmPassword.value) {
				password.setCustomValidity('invalid');
				confifmPassword.setCustomValidity('invalid');
			}
		}

		PronopolyDispatcher.dispatch({
			type: UserActionTypes.USER_UPDATE_FORM,
			name: name,
			value: value
		});
	},
	handleInputChangeForUpdateData(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		if (name === 'password' || name === 'confirmPassword') {
			const password = document.getElementById('password');
			const confifmPassword = document.getElementById('confirmPassword');
			password.setCustomValidity('');
			confifmPassword.setCustomValidity('');

			if (password.value !== confifmPassword.value) {
				password.setCustomValidity('invalid');
				confifmPassword.setCustomValidity('invalid');
			}
		}

		PronopolyDispatcher.dispatch({
			type: UserActionTypes.USER_UPDATE_FORM_UPDATE,
			name: name,
			value: value
		});
	},
	async initUpdateForm() {
		const user = await UserAPI.getConnectedUser();
		PronopolyDispatcher.dispatch({
			type: UserActionTypes.INIT_UPDATE_FORM,
			user: user.connected
		});
	},
	async updateData(firstName, lastName, email, photoUrl) {
		try {
			await UserAPI.updateData(firstName, lastName, email, photoUrl);
			PronopolyDispatcher.dispatch({
				type: UserActionTypes.UPDATE_FORM_DATA_SUCCESS,
				message: "Vos informations sont à jour."
			});
		} catch (ex) {
			PronopolyDispatcher.dispatch({
				type: UserActionTypes.UPDATE_FORM_DATA_ERROR,
				message: "Une erreur est survenue lors de la modification de vos informations."
			});
		}
	},
	async updatePassword(oldPassword, newPassword) {
		try {
			await UserAPI.updatePassword(oldPassword, newPassword);
			PronopolyDispatcher.dispatch({
				type: UserActionTypes.UPDATE_FORM_PASSWORD_SUCCESS,
				message: "Vos informations sont à jour."
			});
		} catch (ex) {
			PronopolyDispatcher.dispatch({
				type: UserActionTypes.UPDATE_FORM_PASSWORD_ERROR,
				message: "Une erreur est survenue lors de la modification de vos informations."
			});
		}
	}

};

export default UserFormActions;