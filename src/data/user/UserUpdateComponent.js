import React from 'react';
import UserUpdateData from './UserUpdateDataComponent';
import UserUpdatePassword from './UserUpdatePasswordComponent';

class UserUpdate extends React.Component {
	render() {
		return (
			<div className="container">
				<h1 className="display-4">Modifier mon profil</h1>
				<div className="row">
					<div className="col-12">
						<UserUpdateData />
					</div>
					<div className="col-12">
						<hr />
					</div>
					<div className="col-12">
						<UserUpdatePassword />
					</div>
				</div>
			</div>
		);
	}
}

export default UserUpdate;