import React from 'react';
import UserFormAction from './UserFormAction';
import UserUpdateStore from './UserUpdateStore';
import FluxComponent from '../../helper/FluxComponent';

class UserUpdatePassword extends FluxComponent {

	constructor(props) {
		super(props, UserUpdateStore);
	}


	handleInputChange = (e) => {
		UserFormAction.handleInputChangeForUpdateData(e);
	}

	handleSubmit = (e) => {
		e.preventDefault();

		let isValid = true;

		const forms = document.getElementsByClassName('form2');
		Array.from(forms).forEach(function (form) {
			if (form.checkValidity() === false) {
				isValid = false;
			}
			form.classList.add('was-validated');
		});

		if (isValid) {
			UserFormAction.updatePassword(this.state.oldPassword, this.state.password);
		}
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit} className="needs-validation form2" noValidate>
				<h3>Modifier mon mot de passe</h3>
				<div className="form-row">
					<div className="form-group col-md-12">
						<label htmlFor="oldPassword">Mot de passe actuel*</label>
						<input type="password" name="oldPassword" id="oldPassword"
							className="form-control" required
							value={this.state.oldPassword} onChange={this.handleInputChange} />
						<div className="invalid-feedback">
							Veuillez saisir votre ancien mot de passe
									</div>
					</div>
					<div className="form-group col-md-6">
						<label htmlFor="password">Nouveau mot de passe*</label>
						<input type="password" name="password" id="password"
							className="form-control" required
							value={this.state.password} onChange={this.handleInputChange} />
						<div className="invalid-feedback">
							Veuillez saisir votre nouveau mot de passe
									</div>
					</div>
					<div className="form-group col-md-6">
						<label htmlFor="confirmPassword">Confirmer votre nouveau mot de passe*</label>
						<input type="password" name="confirmPassword"
							className="form-control" required
							id="confirmPassword" value={this.state.confirmPassword} onChange={this.handleInputChange} />
						<div className="invalid-feedback">
							Veuillez confirmer votre nouveau mot de passe
									</div>
					</div>
				</div>

				<button type="submit" className="btn btn-primary mb-2">Modifier mon mot de passe</button>
				{this.state.successPassword ?
					<div className="alert alert-primary" role="alert">
						Vos informations sont à jour.
					</div> : ''}
				{this.state.errorMessagePassword ?
					<div className="alert alert-danger" role="alert">
						{this.state.errorMessagePassword}
					</div>
					: ''
				}
			</form>
		);
	}
}

export default UserUpdatePassword;