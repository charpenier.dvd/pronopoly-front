import React from 'react';
import UserFormAction from './UserFormAction';
import UserStore from './UserStore';
import FluxComponent from '../../helper/FluxComponent';
import { Link } from 'react-router-dom';

class UserForm extends FluxComponent {
	constructor(props) {
		super(props, UserStore);

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(event) {
		UserFormAction.handleInputChange(event);
	}

	async handleSubmit(event) {
		event.preventDefault();

		let isValid = true;

		const forms = document.getElementsByClassName('needs-validation');
		Array.from(forms).forEach(function (form) {
			if (form.checkValidity() === false) {
				isValid = false;
			}
			form.classList.add('was-validated');
		});

		if (isValid) {
			const user = {
				firstName: this.state.firstName,
				lastName: this.state.lastName,
				email: this.state.email,
				login: this.state.login,
				password: this.state.password,
				photoUrl: this.state.photoUrl,
				activatePageUrl: this.state.activatePageUrl
			}
			UserFormAction.register(user);
		}
	}

	render() {
		return (
			<div className="row justify-content-around mt-4 bg-opacity">
				<div className="col-md-6">
					<div className="card">
						<div className="card-body">
							<h1 className="display-4">Création de compte</h1>
							<form onSubmit={this.handleSubmit} className="needs-validation" noValidate>
								<div className="form-row">
									<div className="form-group col-md-6">
										<label htmlFor="firstName">Prénom*</label>
										<input type="text" id="firstName" name="firstName"
											className="form-control" required
											value={this.state.firstName} onChange={this.handleInputChange} />
										<div className="invalid-feedback">
											Veuillez saisir votre prénom
        								</div>
									</div>
									<div className="form-group col-md-6">
										<label htmlFor="lastName">Nom*</label>
										<input type="text" name="lastName" id="lastName"
											className="form-control" required
											value={this.state.lastName} onChange={this.handleInputChange} />
										<div className="invalid-feedback">
											Veuillez saisir votre nom
        								</div>
									</div>

								</div>

								<div className="form-group">
									<label htmlFor="email">Email*</label>
									<input type="email" name="email" id="email"
										className="form-control" required
										value={this.state.email} onChange={this.handleInputChange} />
									<div className="invalid-feedback">
										Veuillez saisir un email valide
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="photo">Url de votre avatar</label>
									<input type="url" name="photoUrl" id="photo"
										className="form-control" pattern="(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)"
										value={this.state.photoUrl} onChange={this.handleInputChange} />
									<div className="invalid-feedback">
										Veuillez saisir une url d'image valide
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="login">Login*</label>
									<input type="text" name="login" id="login"
										className="form-control" required
										value={this.state.login} onChange={this.handleInputChange} />
									<div className="invalid-feedback">
										Veuillez saisir votre login
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="password">Mot de passe*</label>
									<input type="password" name="password" id="password"
										className="form-control" required
										value={this.state.password} onChange={this.handleInputChange} />
									<div className="invalid-feedback">
										Veuillez saisir votre mot de passe
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="confirmPassword">Confirmer votre mot de passe*</label>
									<input type="password" name="confirmPassword"
										className="form-control" required
										id="confirmPassword" value={this.state.confirmPassword} onChange={this.handleInputChange} />
									<div className="invalid-feedback">
										Veuillez confirmer votre mot de passe
									</div>
								</div>
								<button type="submit" className="btn btn-primary mb-2">Créer son compte</button>
								<Link to='/' className="btn btn-secondary ml-2 mb-2" role="button">Annuler</Link>
								{this.state.success ?
									<div className="alert alert-primary" role="alert">
										Votre compte est crée, veuillez l'activer en cliquant sur le lien présent dans le mail de bienvenue
									</div>
									: ''}
								{this.state.errorMessage ?
									<div className="alert alert-danger" role="alert">
										{this.state.errorMessage}
									</div>
									: ''
								}
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default UserForm;