import {
	ReduceStore
} from 'flux/utils';
import {
	UserActionTypes
} from '../ActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';

class UserActivationStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			success: false,
			errorMessage: ''
		}
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case UserActionTypes.USER_ACTIVATED:
				state.success = true;
				state.errorMessage = 'test';
				this.__emitChange();
				break;
			case UserActionTypes.USER_NOT_ACTIVATED:
				state.success = false;
				state.errorMessage = action.message;
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new UserActivationStore();