import {
	ReduceStore
} from 'flux/utils';
import {
	UserActionTypes
} from '../ActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';
import {
	domain
} from './../../helper/Config';

class UserStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			firstName: '',
			lastName: '',
			email: '',
			login: '',
			password: '',
			confirmPassword: '',
			photoUrl: '',
			success: '',
			errorMessage: '',
			activatePageUrl: `${domain}/activate`
		}
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case UserActionTypes.USER_CREATED:
				state.success = true;
				state.errorMessage = '';
				this.__emitChange();
				break;
			case UserActionTypes.USER_NOT_CREATED:
				state.success = false;
				state.errorMessage = action.message;
				this.__emitChange();
				break;
			case UserActionTypes.USER_UPDATE_FORM:
				state[action.name] = action.value;
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new UserStore();