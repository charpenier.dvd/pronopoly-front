import ApiHelper from '../../helper/ApiHelper'

class UserService extends ApiHelper {

	async register(user) {
		const url = 'user/create';
		const params = JSON.stringify(user);
		return await this.execute(url, 'POST', params);
	}

	async activate(token) {
		const url = 'user/activateAccount';
		const params = JSON.stringify({
			token: token,

		});

		return await this.execute(url, 'POST', params);
	}

	async getConnectedUser() {
		const url = 'user';
		return await this.executeConnected(url, 'GET');
	}

	async updateData(firstName, lastName, email, photoUrl) {
		const url = 'user/update';
		const params = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			email: email,
			photoUrl: photoUrl
		});
		return await this.executeConnected(url, 'POST', params);
	}

	async updatePassword(oldPassword, newPassword) {
		const url = 'user/updatePassword';
		const params = JSON.stringify({
			oldPassword: oldPassword,
			newPassword: newPassword
		});
		return await this.executeConnected(url, 'POST', params);
	}
}

export default new UserService();