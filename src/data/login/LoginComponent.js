import React from 'react';
import { Link } from 'react-router-dom';
import LoginAction from './LoginAction';
import LoginStore from './LoginStore';
import FluxComponent from '../../helper/FluxComponent'

class Login extends FluxComponent {
	constructor(props) {
		super(props, LoginStore);

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(event) {
		LoginAction.handleInputChange(event);
	}

	async handleSubmit(event) {
		event.preventDefault();
		let isValid = true;

		const forms = document.getElementsByClassName('needs-validation');
		Array.from(forms).forEach(function (form) {
			if (form.checkValidity() === false) {
				isValid = false;
			}
			form.classList.add('was-validated');
		});
		if (isValid) {
			LoginAction.login(this.state.login, this.state.password);
		}
	}

	render() {
		return (
			<div className="container">
				<div className="row justify-content-around">
					<div className="col-md-8">
						<h1 className="text-center text-white display-4 mt-2">Bienvenue sur Pronopoly</h1>
					</div>
				</div>
				<div className="row justify-content-around mt-4 bg-opacity">
					<div className="col-md-4">
						<div className="card">
							<div className="card-body">
								<h2>Identifiez-vous</h2>
								<form onSubmit={this.handleSubmit} className="needs-validation" noValidate>
									<div className="form-group">
										<label htmlFor="login">Identifiant</label>
										<input type="text" name="login" id="login" className="form-control"
											value={this.state.login} onChange={this.handleInputChange} required />
										<div className="invalid-feedback">
											Veuillez saisir votre login
        								</div>
									</div>
									<div className="form-group">
										<label htmlFor="login">Mot de passe</label>
										<input type="password" name="password" id="password" className="form-control"
											value={this.state.password} onChange={this.handleInputChange} required />
										<div className="invalid-feedback">
											Veuillez saisir votre mot de passe
        								</div>
									</div>
									<button type="submit" className="btn btn-primary mb-2">Connexion</button>

									{this.state.errorMessage ?
										<div className="alert alert-danger" role="alert">
											{this.state.errorMessage}
										</div>
										: ''
									}
								</form>
								<hr />
								<div className="bd-example">
									<Link to='/register' className="btn btn-primary btn-sm mr-1 mb-1" role="button">Création de compte</Link>
									<Link to='/resetPassword' className="btn btn-secondary btn-sm mb-1" role="button">Mot de passe oublié</Link>
								</div>
							</div>
						</div>



					</div>
				</div>
			</div>

		);
	}
}

export default Login;