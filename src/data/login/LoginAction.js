import LoginActionTypes from './LoginActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';
import AuthService from './LoginAPI';


const LoginActions = {

	async login(login, password) {
		try {
			const result = await AuthService.authenticate(login, password);
			localStorage.setItem('token', result.token);
			PronopolyDispatcher.dispatch({
				type: LoginActionTypes.LOGIN_SUCCESS,
				token: result.token
			});
		} catch (err) {
			PronopolyDispatcher.dispatch({
				type: LoginActionTypes.LOGIN_ERROR,
				message: 'Utilisateur inconnu'
			});
		}
	},
	async disconnect(){
		await AuthService.disconnect();
		PronopolyDispatcher.dispatch({
			type: LoginActionTypes.DISCONNECT
		});
	}
	,
	handleInputChange(event){
		const target = event.target;
		const value = target.value;
		const name = target.name;

		PronopolyDispatcher.dispatch({
			type: LoginActionTypes.UPDATE_LOGIN_FORM,
			name : name,
			value : value
		});
	}

};

export default LoginActions;