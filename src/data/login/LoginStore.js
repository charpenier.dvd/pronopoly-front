import {
	ReduceStore
} from 'flux/utils';
import LoginActionTypes from './LoginActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';

class LoginStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const token = localStorage.getItem('token');
		const obj = {
			login: '',
			password: '',
			errorMessage: '',
			isAuth: token ? true : false
		};
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case LoginActionTypes.LOGIN_SUCCESS:
				state.errorMessage = '';
				state.password = '';
				state.isAuth = true;
				this.__emitChange();
				break;
			case LoginActionTypes.LOGIN_ERROR:
				state.errorMessage = action.message;
				this.__emitChange();
				break;
			case LoginActionTypes.UPDATE_LOGIN_FORM:
				state[action.name] = action.value;
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new LoginStore();