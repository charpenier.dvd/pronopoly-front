import ApiHelper from '../../helper/ApiHelper'

class AuthService extends ApiHelper {

	async authenticate(login, password) {
		const url = 'authenticate';
		const params = JSON.stringify({
			login: login,
			password: password,
		});

		return await this.execute(url, 'POST', params);
	}

	async disconnect(){
		const url ='authenticate/disconnect';
		return await this.executeConnected(url, 'POST');
	}
}

export default new AuthService();