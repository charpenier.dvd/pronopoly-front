import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import LoginAction from './LoginAction';

class Disconnect extends Component {

	componentWillMount(){
		LoginAction.disconnect();
	}

	render() {
		return (
			<div>
				<Redirect to="/" />
			</div>
		);
	}
}

export default Disconnect;