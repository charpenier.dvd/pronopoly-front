import React, { Component } from 'react';
import Teams from '../team/TeamHomeComponent';
import FinalTree from '../FinalTree/FinalTreeComponent';
import Topics from '../topic/TopicComponent';


class Home extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="display-4 d-none d-sm-block">Bienvenue sur Pronopoly!</h1>
        <Topics/>
        <FinalTree/>
        <Teams/>
      </div>
    );
  }
}

export default Home;
