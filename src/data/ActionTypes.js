const MatchActionTypes = {
	MATCH_GET: 'MATCH_GET',
	MATCH_DETAILS_GET: 'MATCH_DETAILS_GET'
};

const LoginActionTypes = {
	LOGIN_SUCCESS: 'LOGIN_SUCESS',
	LOGIN_ERROR: 'LOGIN_ERROR',
	UPDATE_LOGIN_FORM: 'UPDATE_LOGIN_FORM',
	DISCONNECT: 'DISCONNECT'
};

const PronosticActionTypes = {
	PRONOSTIC_GET: 'PRONOSTIC_GET',
	PRONOSTIC_INIT: 'PRONOSTIC_INIT',
	PRONOSTIC_CREATED: 'PRONOSTIC_CREATED',
	PRONOSTIC_UPDATE: 'PRONOSTIC_UPDATE',
	PRONOSTIC_PENDING: 'PRONOSTIC_PENDING',
	PRONOSTIC_ERROR: 'PRONOSTIC_ERROR'
};

const RankingActionTypes = {
	RANKING_INIT: 'RANKING_INIT'
}

const FinalTreeActionTypes = {
	FINAL_TREE_GET: 'FINAL_TREE_GET'
}

const TopicActionTypes = {
	GET_MAIN_TOPIC: 'GET_MAIN_TOPIC',
	GET_REPONSE_TOPIC: 'GET_REPONSE_TOPIC',
	UPDATE_TOPIC: 'UPDATE_TOPIC',
	CREATE_TOPIC: 'CREATE_TOPIC',
	ERROR_TOPIC: 'ERROR_TOPIC',
	DELETE_TOPIC: 'DELETE_TOPIC',
	NEXT_PAGE: 'NEXT_PAGE',
	PREVIOUS_PAGE: 'PREVIOUS_PAGE'
}

const ResetPasswordActionTypes = {
	RESET_PASSWORD_REQUEST: 'RESET_PASSWORD_REQUEST',
	RESET_PASSWORD_UPDATE: 'RESET_PASSWORD_UPDATE',
	RESET_PASSWORD_FORM_UPDATE: 'RESET_PASSWORD_FORM_UPDATE',
	RESET_PASSWORD: 'RESET_PASSWORD',
	RESET_PASSWORD_SUCCESS: 'RESET_PASSWORD_SUCCESS',
	RESET_PASSWORD_ERROR: 'RESET_PASSWORD_ERROR'
}

const UserActionTypes = {
	USER_CREATED: 'USER_CREATED',
	USER_NOT_CREATED: 'USER_NOT_CREATED',
	USER_UPDATE_FORM: 'USER_UPDATE_FORM',
	USER_ACTIVATED: 'USER_ACTIVATED',
	USER_NOT_ACTIVATED: 'USER_NOT_ACTIVATED',
	INIT_UPDATE_FORM: 'INIT_UPDATE_FORM',
	USER_UPDATE_FORM_UPDATE: 'USER_UPDATE_FORM_UPDATE',
	UPDATE_FORM_DATA_SUCCESS: 'UPDATE_FORM_DATA_SUCCESS',
	UPDATE_FORM_DATA_ERROR: 'UPDATE_FORM_DATA_ERROR',
	UPDATE_FORM_PASSWORD_SUCCESS: 'UPDATE_FORM_PASSWORD_SUCCESS',
	UPDATE_FORM_PASSWORD_ERROR: 'UPDATE_FORM_PASSWORD_ERROR'
}


export {
	MatchActionTypes,
	LoginActionTypes,
	PronosticActionTypes,
	RankingActionTypes,
	FinalTreeActionTypes,
	TopicActionTypes,
	ResetPasswordActionTypes,
	UserActionTypes
};