import ApiHelper from '../../helper/ApiHelper'

class TeamService extends ApiHelper {

	
	async getTeams(){
		const url = 'team/all';
		return await this.executeConnected(url, 'GET');
	}
}

export default new TeamService();