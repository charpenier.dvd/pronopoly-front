import TeamActionTypes from './TeamActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';
import TeamAPI from './TeamAPI';
import {
	AuthenticationException
} from './../../exception/Exception';
import LoginActionTypes from './../login/LoginActionTypes';


const TeamActions = {

	async getTeamsByGroups() {
		try {
			const result = await TeamAPI.getTeams();
			let teams = {};

			const sortFunction = function (team1, team2) {
				const team1Diff = parseInt(team1.scored_goals, 10) - parseInt(team1.taken_goals, 10);
				const team2Diff = parseInt(team2.scored_goals, 10) - parseInt(team2.taken_goals, 10);
				return team1.points === team2.points ? team2Diff - team1Diff : team2.points - team1.points;
			}

			teams.A = result.teams.filter(x => x.group_letter === 'A').sort(sortFunction);
			teams.B = result.teams.filter(x => x.group_letter === 'B').sort(sortFunction);
			teams.C = result.teams.filter(x => x.group_letter === 'C').sort(sortFunction);
			teams.D = result.teams.filter(x => x.group_letter === 'D').sort(sortFunction);
			teams.E = result.teams.filter(x => x.group_letter === 'E').sort(sortFunction);
			teams.F = result.teams.filter(x => x.group_letter === 'F').sort(sortFunction);
			teams.G = result.teams.filter(x => x.group_letter === 'G').sort(sortFunction);
			teams.H = result.teams.filter(x => x.group_letter === 'H').sort(sortFunction);

			PronopolyDispatcher.dispatch({
				type: TeamActionTypes.TEAM_GET,
				teams: teams
			});
		} catch (err) {
			if (err instanceof AuthenticationException) {
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
		}
	}

};

export default TeamActions;