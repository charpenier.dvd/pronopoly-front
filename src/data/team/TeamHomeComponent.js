import React from 'react';
import TeamActions from './TeamActions';
import TeamStore from './TeamStore';
import FluxComponent from '../../helper/FluxComponent';
import TeamByGroup from './TeamByGroup'

class Teams extends FluxComponent {
	constructor(props) {
		super(props, TeamStore);
	}

	componentDidMount() {
		super.componentDidMount();
		TeamActions.getTeamsByGroups();
	}

	render() {
		const { teams } = this.state;
		return (
			<div>
				<h2>Les poules</h2>
				<hr/>
				<div className="row">
					{Object.keys(teams).map((gps, index) =>
						<TeamByGroup groupLetter={gps} {...this.state} key={index} />
					)}
				</div>
			</div>
		);
	}
}

export default Teams;