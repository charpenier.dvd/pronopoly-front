import {
	ReduceStore
} from 'flux/utils';
import TeamActionTypes from './TeamActionTypes'
import PronopolyDispatcher from '../PronopolyDispatcher';

class TeamStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			teams : []
		};
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case TeamActionTypes.TEAM_GET:
				state.teams = action.teams;
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new TeamStore();