import React, { Component } from 'react';

class TeamByGroup extends Component {

	constructor(props) {
		super(props);
		this.state = {
			teams: this.props.teams[this.props.groupLetter],
			groupLetter: this.props.groupLetter
		}
	}

	render() {
		return (
			<div className="col-md-6 col-sm-12">
				<h3>Groupe {this.state.groupLetter}</h3>
				<div className="table-responsive">
					<table className="table table-sm">
						<thead>
							<tr>
								<th>Equipe</th>
								<th>Pts</th>
								<th>J.</th>
								<th>G.</th>
								<th>N.</th>
								<th>P.</th>
								<th>p.</th>
								<th>c.</th>
								<th>Diff.</th>
							</tr>
						</thead>
						<tbody>
							{this.state.teams.map(function (team, index) {
								return <tr key={index}>
									<td>
										<img src="/flag/blank.gif" className={"flag mr-2 flag-" + team.flag_path} alt={team.flag_path} />
										{team.name}
									</td>
									<td>{team.points}</td>
									<td>{parseInt(team.nb_victories, 10) +
										parseInt(team.nb_nulls, 10) +
										parseInt(team.nb_defeats, 10)}</td>
									<td>{team.nb_victories}</td>
									<td>{team.nb_nulls}</td>
									<td>{team.nb_defeats}</td>
									<td>{team.scored_goals}</td>
									<td>{team.taken_goals}</td>
									<td>{parseInt(team.scored_goals, 10) - parseInt(team.taken_goals, 10)}</td>
								</tr>
							})}
						</tbody>
					</table>
				</div>
			</div>
		)
	}
}

export default TeamByGroup;