import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Nav extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg navbar-light bg-light mb-3">
				<Link className="navbar-brand" to='/home'>Accueil</Link>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item"><Link className="nav-link" to='/match'>Match</Link></li>
						<li className="nav-item"><Link className="nav-link" to='/pronostic'>Pronostics</Link></li>
						<li className="nav-item"><Link className="nav-link" to='/ranking'>Classement</Link></li>
						<li className="nav-item"><Link className="nav-link" to='/rules'>Règles</Link></li>
						<li className=""></li>
					</ul>
					<div className="my-2 my-lg-0">
						<Link className="btn btn-outline-info mr-2" to='/profile'>Profil</Link>
						<Link className="btn btn-outline-danger" to='/disconnect'>Déconnexion</Link>
					</div>
				</div>
			</nav>
		);
	}
}

export default Nav;

