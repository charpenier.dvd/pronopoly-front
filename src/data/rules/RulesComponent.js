import React, { Component } from 'react';

class Rules extends Component {
	render() {
		return (
			<div className="container">
				<h1 className="display-4">Règles</h1>
				<p>
					Cette page explique en détails <strong>les règles utilisées pour le calcul des points.</strong>
					Si vous êtes là pour gagner, lisez-les attentivement !
					</p>
				<p>
					Comme vous le savez, la coupe du monde se déroule en deux phases, la phase de groupe et la phase
	à élimination directe. Si vous souhaitez vous renseigner un peu plus sur le format de la coupe du
	monde 2018, les règles sont décrites <a rel="noopener noreferrer" href="https://resources.fifa.com/image/upload/coupe-du-monde-de-la-fifa-russie-2018tm-reglements-2843521.pdf?cloudid=ktsdzsydik9ve2qx190g" target="_blank">ici</a>.
				</p>
				<h2>Phase de groupe</h2>
				<p>
					Lors des phases de groupes, les matchs durent 90 minutes ( + temps additionnel) et peuvent se
terminer sur un match nul.
Le score que vous aurez pronostiqué sera comparé au score final pour déterminer si vous avez trouvé:</p>
				<ol>
					<li>Le bon vainqueur (2 points)</li>
					<li>Le bon écart de buts (2 points)</li>
					<li>Le bon nombre de but marqué par une équipe (1 point par équipe)</li>
					<li>Le score parfait (1 point)</li>
				</ol>
				<p>
					Après avoir lu ça, mille questions vous traversent l'esprit, ne vous inquiétez pas, nous allons y répondre de suite.
				</p>
				<p>
					<strong>Je peux gagner que 2 points par match ? C'est nul !</strong><br />
					=> Détrompes toi, les règles sont cumulatives, c'est à dire qu'elles peuvent s'appliquer en même temps pour un même pronostic.
				</p>
				<p>
					<strong>Le bon vainqueur ? Mais si c'est un match nul, comment kon fé ?</strong><br />
					=> Pendant la phase de groupe, si vous pronostiquez un match nul, et que le match résulte effectivement en un match nul, vous gagnez les points du bon vainqueur (et du bon écart de but à coup sûr)
				</p>
				<p>
					<strong>Le bon écart de but fonctionne-t-il si on a mis par exemple 3-1 et que le score final est 1-3 ?</strong><br />
					=> non, l'écart de but est signé. Autrement dit, il faut que le score de l'équipe 1 moins le score de l'équipe 2 donne le même résultat pour votre pronostic que pour le score final.
				</p>
				<p>
					<strong>Le bon nombre de but pour une équipe me rapporte-t-il un point si je n'ai pas le bon vainqueur ?</strong><br />
					=> oui, si vous pronostiquez 1-0, et que le score réel du match est 1-3, vous marquez un point pour avoir trouvé que l'équipe 1 allait marquer exactement 1 but. On est comme ça nous, on est plutôt sympa.
				</p>
				<p>
					<strong>Si j'ai pronostiqué le score exact, je gagne combien de points exactement ?</strong><br />
					=> Posons ce calcul très complexe :</p>
				<ul>
					<li>2 (bon vainqueur)</li>
					<li>+ 2 (bon écart)</li>
					<li>+ 1 (bon nb de buts équipe 1)</li>
					<li>+ 1 (bon nb de buts équipe 2)</li>
					<li>+ 1 (score parfait)</li>
				</ul>
				<p>
					----------------------------------------<br />
					7 points
				</p>
				<p>
					<strong>Mais si je trouve, le bon vainqueur, le bon écart de but et le bon nombre de buts marqués, j'ai forcément le score parfait non ?</strong><br />
					=> exactement, comme trouver un score exacte c'est quand même la méga classe, on vous a rajouté le point du score parfait.
				</p>
				<h2>Phase à élimination directe</h2>
				<p>
					En phase à élimination directe, les matchs peuvent durer 90 minutes, 120 minutes voire aller au penalties.
				</p>
				<p>
					Les règles pour le calcul de points sont identiques à la phase de groupe, mais les conditions d'applications sont du coup un peu plus complexes.<br />
				</p>
				<p>Vous devrez pronostiquer le score <strong>final</strong> du match.<br />
					S'il y a des prolongations, (égalité au bout de 90 minutes), c'est le score au bout de 120 minutes qui compte pour l'application des règles de décompte des points.<br />
				</p>
				<p>
					Contrairement à ce qu'il se passe en phases de groupe, les matchs en phase à élimination directe ont systématiquement un vainqueur. Si vous pronostiquez match nul, c'est donc que vous pensez que le match se terminera aux penalties. Dans ce cas, vous devrez sélectionner l'équipe qui selon vous gagnera le match aux tirs au but. Vous bénéficierez alors du "bon vainqueur" si l'équipe sélectionnée est celle qui remporte le match.<br />
					Comme pour la phase de groupe, voici une série de questions/réponses pour éclaircir les points litigieux :
				</p>
				<p>
					<strong>J'ai rien compris… Tu peux reformuler ?</strong><br />
					=> mais bien sûr :
				</p>
				<ul>
					<li>Si à l'issu des 90 minutes, l'une des deux équipes a gagné le match, tout se passe comme en phase de poule.</li>
					<li>Si à l'issu des 90 minutes, il y a match nul, il y a donc des prolongations. C'est le score à la fin des prolongations qui est comparé au pronostic.</li>
					<li>Si à la fin des prolongations, il y a encore égalité, les équipes procèdent aux tirs au but. Le vainqueur est alors déterminé, mais les buts marqués pendant la séance de tirs au but ne comptent pas dans l'application des règles.</li>
				</ul>
				<p>
					<strong>Du coup si je comprends bien, si je pronostique 3-2 et qu'il y a 2-2 à la fin des prolongations puis 5-4 aux pénaltys  je marque des points c'est ça ?</strong><br />
					=> exactement, tu auras le bon vainqueur, et aussi le bon nombre de but marqué pour l'équipe 2, soit un total de 3 points. En revanche, le bon écart de but n’est pas accordé car les buts marqués lors des tirs au but ne sont pas comptabilisés.
				</p>
				<p>
					<strong>Si je pronostique 2-2, et que le match se déroule comme suit : 2-2 à la 90ème puis 4-3 à la fin de prolongations je marque des points ?</strong><br />
					=> non, aucun point.
				</p>
			</div>
		);
	}
}

export default Rules;

