import React from 'react';
import { Switch, Route } from 'react-router-dom';
import RouterStore from './RouterStore';
import FluxComponent from '../../helper/FluxComponent';
import UserRegistration from '../../data/user/UserFormComponent';
import Nav from '../nav/NavComponent';
import Login from '../login/LoginComponent';
import Home from '../home/Home';
import Disconnect from '../login/DisconnectComponent';
import PrivateRoute from './PrivateRouteComponent';
import PronosticList from '../pronostic/PronosticListComponent';
import MatchList from '../match/MatchListComponent';
import MatchDetailsComponent from '../match/MatchDetailsComponent';
import Ranking from '../ranking/RankingComponent';
import ResetPasswordComponent from '../resetPassword/ResetPasswordComponent';
import ResetPasswordFormComponent from '../resetPassword/ResetPasswordFormComponent';
import UserActivation from '../user/UserActivationComponent';
import UserUpdate from '../user/UserUpdateComponent';
import Rules from '../rules/RulesComponent';

class RouterComponent extends FluxComponent {
	constructor(props) {
		super(props, RouterStore);
	}

	componentWillMount(){
		let body = document.getElementsByTagName('body')[0];
		if(this.state.isAuth){
			 body.classList.remove('bgHome');
		} else {
			body.classList.add('bgHome');
		}
	}

	render() {
		return (
			<div>
				{this.state.isAuth ? <Nav /> : ''}
				{
					this.state.isAuth ?
						<Switch>
							<Route exact path='/pronostic' component={PronosticList} />
							<Route exact path='/match' component={MatchList} />
							<Route exact path='/match/details/:id' component={MatchDetailsComponent} />
							<Route exact path='/ranking' component={Ranking} />
							<Route exact path='/profile' component={UserUpdate} />
							<Route exact path='/rules' component={Rules} />
							<Route exact path='/disconnect' component={Disconnect} />
							<PrivateRoute component={Home} />
						</Switch> :
						
						<Switch>
							<Route exact path='/register' component={UserRegistration} />
							<Route exact path='/resetPassword' component={ResetPasswordComponent} />
							<Route exact path='/activate' component={UserActivation}/>
							<Route exact path='/resetPasswordForm' component={ResetPasswordFormComponent}/>
							<Route component={Login} />
						</Switch>
				}
			</div>
		);
	}
}

export default RouterComponent;