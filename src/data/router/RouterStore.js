import {
	ReduceStore
} from 'flux/utils';
import LoginActionTypes from '../login/LoginActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';

class RouterStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const token = localStorage.getItem('token');
		const obj = {
			isAuth: token ? true : false
		};
		return obj;
	}

	reduce(state, action) {
		let body = document.getElementsByTagName('body')[0];
		switch (action.type) {
			case LoginActionTypes.LOGIN_SUCCESS:
				state.isAuth = true;
				body.classList.remove('bgHome');
				this.__emitChange();
				break;
			case LoginActionTypes.DISCONNECT:
				localStorage.removeItem('token');
				body.classList.add('bgHome');
				state.isAuth = false;
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new RouterStore();