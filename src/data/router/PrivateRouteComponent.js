import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import FluxComponent from '../../helper/FluxComponent';
import RouterStore from './RouterStore'

class PrivateRoute extends FluxComponent {
	constructor(props) {
		super(props, RouterStore);
	}

	render() {
		const { component: Component, ...rest } = this.props;
		return (
			<Route
				{...rest}
				render={props =>
					this.state.isAuth ? (
						<Component {...props} />
					) : (
							<Redirect
								to={{
									pathname: "/",
									state: { from: props.location }
								}}
							/>
						)
				}
			/>
		);
	}
}

export default PrivateRoute;