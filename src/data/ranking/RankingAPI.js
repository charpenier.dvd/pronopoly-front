import ApiHelper from '../../helper/ApiHelper'

class RankingService extends ApiHelper {

	async getPointsRanking(){
		const url = 'ranking/points';
		return await this.executeConnected(url, 'GET');
	}
	
}

export default new RankingService();