import React from 'react';
import RankingActions from './RankingActions';
import RankingStore from './RankingStore';
import FluxComponent from '../../helper/FluxComponent';

class RankingComponent extends FluxComponent {
	constructor(props) {
		super(props, RankingStore);
	}

	componentWillMount() {
		RankingActions.getRanking();
	}

	getImgStyle(index) {
		switch (index) {
			case 0:
				return { width: '45px', height: 'auto' };
			case 1:
				return { width: '40px', height: 'auto' };
			case 2:
				return { width: '35px', height: 'auto' };
			default:
				return {
					width: '30px',
					height: 'auto'
				};
		}
	}

	render() {
		const { ranking } = this.state;

		return (
			<div className="container">
				<h1 className="display-4">Classement</h1>
				<div className="table-responsive">
					<table className="table table-striped table-sm">
						<tbody>
							{ranking.map((user, index) => {
								return <tr key={index}>
									<td className={index < 3 ? "align-middle h" + (index + 2) : "align-middle"}>
										{index + 1}
									</td>
									<td className={index < 3 ? "align-middle h" + (index + 2) : "align-middle"}>
										<img src={user.photo_url ? user.photo_url : "/img/user.png"} 
											className="img-thumbnail mr-2"
											style={this.getImgStyle(index)} alt="" />
										<span>{user.first_name}</span>
										<span> {user.last_name}</span>

									</td>
									<td className={index < 3 ? "align-middle h" + (index + 2) : "align-middle"}>
										{user.total_points} pts
									</td>
								</tr>
							}
							)}
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

export default RankingComponent;