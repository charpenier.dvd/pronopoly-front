import {
	ReduceStore
} from 'flux/utils';
import PronopolyDispatcher from '../PronopolyDispatcher';
import {LoginActionTypes, RankingActionTypes} from '../ActionTypes';

class RankingStore extends ReduceStore {
	constructor(prono) {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			ranking : []
		};
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case RankingActionTypes.RANKING_INIT:
				state.ranking = action.ranking;
				this.__emitChange();
				break;
			case LoginActionTypes.DISCONNECT:
				state = this.getInitialState();	
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new RankingStore();