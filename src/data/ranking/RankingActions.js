import {RankingActionTypes, LoginActionTypes} from '../ActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';
import RankingAPI from './RankingAPI';
import {AuthenticationException} from './../../exception/Exception';

const RankingActions = {

	async getRanking() {
		try {
			const ranking = await RankingAPI.getPointsRanking();

			PronopolyDispatcher.dispatch({
				type: RankingActionTypes.RANKING_INIT,
				ranking: ranking.ranking
			});

		} catch (err) {
			if(err instanceof AuthenticationException){
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
		}
	}
};

export default RankingActions;