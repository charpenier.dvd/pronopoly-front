import {
	ReduceStore
} from 'flux/utils';
import {
	TopicActionTypes
} from './../ActionTypes'
import PronopolyDispatcher from '../PronopolyDispatcher';

class NewTopicStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			topics_message: "",
			topic_type: "",
			parentFeed:"",
			status : ""
		};
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case TopicActionTypes.UPDATE_TOPIC:
				state[action.name] = action.value;
				this.__emitChange();
				break;
			case TopicActionTypes.CREATE_TOPIC:
				state.topic_type = "";
				state.topics_message = "";
				state.parentFeed = "";
				state.status = "SUCCESS";
				this.__emitChange();
				break;
			case TopicActionTypes.DELETE_TOPIC:
				//state.topics = action.topics;
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new NewTopicStore();