import {
	ReduceStore
} from 'flux/utils';
import {
	TopicActionTypes
} from './../ActionTypes'
import PronopolyDispatcher from '../PronopolyDispatcher';

class TopicStore extends ReduceStore {
	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			topics: [],
			displayTopics: [],
			currentPage: 0,
			numberOfFeedByPage: 5,
			numberOfPage: null,
			nextMatch: [],
			lastMatch: []
		};
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case TopicActionTypes.GET_MAIN_TOPIC:
				state.topics = action.topics;
				state.displayTopics = action.topics.slice(0, state.numberOfFeedByPage);
				state.numberOfPage = Math.floor(action.topics.length / state.numberOfFeedByPage);
				state.currentPage = 0;
				state.nextMatch  = action.nextMatch;
				state.lastMatch  = action.lastMatch;
				this.__emitChange();
				break;
			case TopicActionTypes.NEXT_PAGE:
				state.currentPage--;
				state.displayTopics = state.topics.slice(state.currentPage * state.numberOfFeedByPage, state.currentPage * state.numberOfFeedByPage + state.numberOfFeedByPage);
				this.__emitChange();
				break;
			case TopicActionTypes.PREVIOUS_PAGE:
				state.currentPage++;
				state.displayTopics = state.topics.slice(state.currentPage * state.numberOfFeedByPage, state.currentPage * state.numberOfFeedByPage + state.numberOfFeedByPage);
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new TopicStore();