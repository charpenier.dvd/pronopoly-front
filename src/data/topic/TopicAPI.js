import ApiHelper from '../../helper/ApiHelper'

class TopicService extends ApiHelper {

	async getMainTopic(){
		const url = 'topic/all';
		return await this.executeConnected(url, 'GET');
	}

	async getMatchs(){
		const url = 'match/all';
		return await this.executeConnected(url, 'GET');
	}

	async getReponseTopic(id){
		const url = `topic/responseFeed/${id}`;
		return await this.executeConnected(url, 'GET');
	}

	async deleteTopic(id){
		const url = `topic/${id}`;
		return await this.executeConnected(url, 'DELETE');
	}

	async postTopic(description, parentFeed, topic_type){
		const url = 'topic/create';
		const param = JSON.stringify({
			description : description,
			parentFeed : parentFeed,
			feed_type : topic_type
		});
		return await this.executeConnected(url,'POST', param);
	}
	
}

export default new TopicService();