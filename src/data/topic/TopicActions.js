import PronopolyDispatcher from '../PronopolyDispatcher';
import TopicAPI from './TopicAPI';
import {
	AuthenticationException
} from './../../exception/Exception';
import {
	LoginActionTypes,
	TopicActionTypes
} from './../ActionTypes';


const TopicActions = {

	async getMainTopics() {
		try {
			const topics = await TopicAPI.getMainTopic();
			const matchTopic = await TopicAPI.getMatchs();
			const nextMatch = matchTopic.matchs.filter(match => new Date(match.date).getTime() > new Date().getTime());
			const lastMatch = matchTopic.matchs.filter(match => new Date(match.date).getTime() < new Date().getTime());
			PronopolyDispatcher.dispatch({
				type: TopicActionTypes.GET_MAIN_TOPIC,
				topics: topics.feeds,
				nextMatch: nextMatch.slice(0, 3),
				lastMatch: lastMatch.length > 2 ? lastMatch.slice(lastMatch.length - 2, lastMatch.length) : lastMatch
			});
		} catch (err) {
			if (err instanceof AuthenticationException) {
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
		}
	},
	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		PronopolyDispatcher.dispatch({
			type: TopicActionTypes.UPDATE_TOPIC,
			name: name,
			value: value
		});
	},
	async createFeed(description, parentFeed, topic_type) {
		const response = await TopicAPI.postTopic(description, parentFeed, topic_type);
		if (response) {
			PronopolyDispatcher.dispatch({
				type: TopicActionTypes.CREATE_TOPIC
			});
			this.getMainTopics();
		} else {
			PronopolyDispatcher.dispatch({
				type: TopicActionTypes.ERROR_TOPIC,
			});
		}
	},
	nextPage() {
		PronopolyDispatcher.dispatch({
			type: TopicActionTypes.NEXT_PAGE
		});
	},
	previousPage() {
		PronopolyDispatcher.dispatch({
			type: TopicActionTypes.PREVIOUS_PAGE
		});
	},
	async deleteTopic(id) {
		await TopicAPI.deleteTopic(id);
		PronopolyDispatcher.dispatch({
			type: TopicActionTypes.DELETE_TOPIC
		});
		this.getMainTopics();
	}
};

export default TopicActions;