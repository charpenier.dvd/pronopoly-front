import React from 'react';
import TopicActions from './TopicActions';
import NewTopicStore from './NewTopicStore';
import FluxComponent from '../../helper/FluxComponent';

class NewTopic extends FluxComponent {
	constructor(props) {
		super(props, NewTopicStore);
	}

	handleInputChange(event) {
		TopicActions.handleInputChange(event);
	}

	handleSubmit = (event) => {
		event.preventDefault();
		TopicActions.createFeed(this.state.topics_message, null, "FEED");
	}

	render() {
		return (
			<form onSubmit={this.handleSubmit} className="text-right mb-2">
				<div className="form-group">
					<textarea className="form-control" id="feed"
						name="topics_message" maxLength="255" required
						value={this.state.topics_message} rows="3"
						onChange={this.handleInputChange}></textarea>
				</div>
				<button type="submit" className="btn btn-sm btn-primary">Envoyer un message</button>
			</form>
		);
	}
}

export default NewTopic;