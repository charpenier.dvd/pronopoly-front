import React from 'react';
import TopicActions from './TopicActions';
import TopicStore from './TopicStore';
import FluxComponent from '../../helper/FluxComponent';
import NewTopic from './NewTopicComponent';
import { getFeedClass, getLinkColor, tradGroupDetail, isTeamWinner } from '../../helper/ComponentHelper';

class Topics extends FluxComponent {
	constructor(props) {
		super(props, TopicStore);
	}

	componentDidMount() {
		super.componentDidMount();
		TopicActions.getMainTopics();
	}

	nextPage(e) {
		e.preventDefault();
		TopicActions.nextPage();
	}

	previousPage(e) {
		e.preventDefault();
		TopicActions.previousPage();
	}

	deleteTopic(e, id) {
		e.preventDefault();
		TopicActions.deleteTopic(id);
	}


	render() {
		const { displayTopics, nextMatch, lastMatch } = this.state;
		// const imgStyle = {
		// 	width: '50px',
		// 	height: '50px'
		// };
		return (
			<div className="mb-md-2">
				<h2>Fil d'actualités</h2>
				<hr />
				<div className="container">
					<div className="row">
						<div className="col-12">
							<NewTopic />
						</div>
						<div className="col-md-6 col-12">
							{lastMatch.length === 0 ? "" : <h3>Derniers résultats</h3>}
							{lastMatch.map((match, index) => {
								const fontTeamWinner1 = isTeamWinner(match.match_type, match.team_1_score, match.team_2_score, match.team_1_score_penalty, match.team_2_score_penalty);
								const fontTeamWinner2 = isTeamWinner(match.match_type, match.team_2_score, match.team_1_score, match.team_2_score_penalty, match.team_1_score_penalty);
								return <div className="card mb-2" key={index}>
									<div className="card-body row">
										<div className="col-12 mb-md-2">
											<p className="card-text lead text-center">
												<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_1_flag} alt={match.team_1_flag} />
												<span className={fontTeamWinner1}>
													{match.team_1_name} {match.team_1_score} {match.match_type !== "Group" && match.team_2_score === match.team_1_score && match.team_1_score ?
														<span>(<span className={fontTeamWinner1}>{match.team_1_score_penalty}</span>)</span>
														: ""}</span>
												<span> - </span>
												<span className={fontTeamWinner2}>{match.match_type !== "Group" && match.team_2_score === match.team_1_score && match.team_1_score ?
													<span>(<span className={fontTeamWinner2}>{match.team_2_score_penalty}</span>)</span>
													: ""} {match.team_2_score} {match.team_2_name}</span>
												<img src="/flag/blank.gif" className={"ml-2 flag flag-" + match.team_2_flag} alt={match.team_2_flag} />
											</p>
										</div>
										<div className="col-md-6 col-12 text-muted">
											<small>{tradGroupDetail(match.match_type)}</small>
										</div>
										<div className="col-md-6 col-12 text-right text-muted">
											<small>{new Date(match.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}</small>
										</div>
									</div>
								</div>
							})}
							{nextMatch.length === 0 ? "" : <h3>Prochains matchs</h3>}
							{nextMatch.map((match, index) => {
								return <div className="card mb-2" key={index}>
									<div className="card-body row">
										<div className="col-12 mb-md-2">
											<p className="card-text lead text-center">
												<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_1_flag} alt={match.team_1_flag} />
												<span>{match.team_1_name}</span>
												<span> - </span>
												<span>{match.team_2_name}</span>
												<img src="/flag/blank.gif" className={"ml-2 flag flag-" + match.team_2_flag} alt={match.team_2_flag} />
											</p>
										</div>
										<div className="col-md-6 col-12 text-muted">
											<small>{tradGroupDetail(match.match_type)}</small>
										</div>
										<div className="col-md-6 col-12 text-right text-muted">
											<small>{new Date(match.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}</small>
										</div>
									</div>
								</div>
							})}
						</div>
						<div className="col-12 col-md-6">
							{displayTopics.map((topic, index) => {
								return <div className={`card ${getFeedClass(topic)}  mb-2`} key={index}>
									<div className="card-body row">
										<div className="col-12 mb-md-2">
											<p className="card-text">{topic.text}</p>
										</div>
										<div className="col-md-6 col-12 d-flex align-items-center">
											{/* <button className={`btn btn-link btn-sm ${getLinkColor(topic)}`}>Repondre</button> */}
											{topic.candelete === "True" || topic.candeleteadm === "True" ? <button className={`btn btn-link btn-sm ${getLinkColor(topic)}`}
												onClick={(e) => this.deleteTopic(e, topic.id)}>
												Supprimer</button> : ""}
										</div>
										<div className="col-md-6 col-12 text-right">
											<div className="row">
												<div className="col-9">
													<div className="row">
														<div className="col-12">
															<small className="text-capitalize">{topic.user_first_name} {topic.user_last_name}</small><br />
														</div>
														<div className="col-12">
															<small>{new Date(topic.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}</small>
														</div>
													</div>
												</div>
												<div className="col-3 text-center p-0">
													<img src={topic.user_photo_url ? topic.user_photo_url : "/img/user.png"} className="img-thumbnail"
														 alt="" /></div>
											</div>
										</div>
									</div>
								</div>
							})}
						</div>
						<div className="offset-md-6 col-6">
							<nav aria-label="Page navigation example">
								<ul className="pagination">
									{this.state.currentPage === 0 ? "" : <li className="page-item"><a href="" className="page-link" onClick={this.nextPage}>Plus récent</a></li>}
									{this.state.currentPage === this.state.numberOfPage ? "" : <li className="page-item"><a href="" className="page-link" onClick={this.previousPage}>Plus ancien</a></li>}
								</ul>
							</nav>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

export default Topics;