import React from 'react';
import MatchActions from './MatchActions';
import MatchDetailsStore from './MatchDetailsStore';
import FluxComponent from '../../helper/FluxComponent';
import { tradGroupDetail, isTeamWinner, pronoTeamWinner } from '../../helper/ComponentHelper';

class MatchDetailsComponent extends FluxComponent {
	constructor(props) {
		super(props, MatchDetailsStore);
		this.matchId = this.props.match.params.id;
	}

	componentDidMount() {
		super.componentDidMount();
		MatchActions.getMatchDetails(this.matchId);
	}

	render() {
		const { match, pronostics } = this.state;
		const style = {
			width: '30px',
			height: '30px'
		};
		const isTeamWinner1 = isTeamWinner(match.match_type, match.team_1_score, match.team_2_score, match.team_1_score_penalty, match.team_2_score_penalty);
		const isTeamWinner2 = isTeamWinner(match.match_type, match.team_2_score, match.team_1_score, match.team_2_score_penalty, match.team_1_score_penalty);
		return (
			<div>
				<div className="jumbotron jumbotron-fluid">
					<div className="container">
						<h1 className="display-4 text-center">
							<span className={isTeamWinner1}>{match.team_1_name}</span> <span className={isTeamWinner1}>{match.team_1_score}</span> - <span className={isTeamWinner2}>{match.team_2_score}</span>
							<span className={isTeamWinner2}> {match.team_2_name}</span>
						</h1>
						{
							match.team_1_score === match.team_2_score && match.match_type !== "Group" ?
								<p className="lead text-center font-italic">Résultats des tirs au but {match.team_1_score_penalty} - {match.team_2_score_penalty}</p> : ""
						}
						<hr />
						<p className="lead text-center">
							{tradGroupDetail(match.match_type)} du {new Date(match.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })} à {match.city}
						</p>
					</div>
				</div>
				<div className="container">
					{pronostics.map(function (prono, index) {
						const pronoTeam1Win = pronoTeamWinner(match.match_type, prono.pronostic_team_1_score, 
							prono.pronostic_team_2_score, match.team_1_id, prono.pronostic_winner);
						const pronoTeam2Win = pronoTeamWinner(match.match_type, prono.pronostic_team_2_score, 
							prono.pronostic_team_1_score, match.team_2_id, prono.pronostic_winner);
						return <div className={index % 2 === 0 ? "row bg-light align-items-center" : "row align-items-center"} key={index}>
							<div className="col-md-4 col-12">
								<img src={prono.user_photo_url === null ? "/img/user.png" : prono.user_photo_url}
									className="img-thumbnail mr-2 m-2" style={style} alt="" />
								<span>{prono.user_first_name}</span>
								<span> {prono.user_last_name}</span>
							</div>
							<div className={`col-md-2 text-right d-none d-sm-block ${pronoTeam1Win}`}>
								{prono.team_1_name}
								<img src="/flag/blank.gif" className={"ml-2 flag flag-" + prono.team_1_flag} alt={prono.team_1_flag} />
							</div>
							<div className="col-md-2 text-center d-none d-sm-block">
								{prono.pronostic_team_1_score}
								<span> - </span>
								{prono.pronostic_team_2_score}
							</div>
							<div className={`col-md-2 text-right d-none d-sm-block ${pronoTeam2Win}`}>
								<img src="/flag/blank.gif" className={"mr-2 flag flag-" + prono.team_2_flag} alt={prono.team_2_flag} />
								{prono.team_2_name}
							</div>
							<div className="col-md-2 text-right d-none d-sm-block">
								{prono.pronostic_points} pts
							</div>

							<div className="col-10 d-block d-sm-none">
								<img src="/flag/blank.gif" className={"mr-2 flag flag-" + prono.team_1_flag} alt={prono.team_1_flag} />
								{prono.team_1_name}
							</div>
							<div className="col-2 d-block d-sm-none">
								{prono.pronostic_team_1_score}
							</div>

							<div className="col-10 d-block d-sm-none">
								<img src="/flag/blank.gif" className={"mr-2 flag flag-" + prono.team_2_flag} alt={prono.team_2_flag} />
								{prono.team_2_name}
							</div>
							<div className="col-2 d-block d-sm-none">
								{prono.pronostic_team_2_score}
							</div>

							<div className="col-12 d-block d-sm-none text-center mb-2">
								<hr />
								{prono.pronostic_points} points
							</div>

						</div>
					})}
				</div>
			</div>
		);
	}
}

export default MatchDetailsComponent;