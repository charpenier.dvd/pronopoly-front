import ApiHelper from '../../helper/ApiHelper'

class MatchService extends ApiHelper {

	async getNextMatchs(){
		const url = 'match/all';
		return await this.executeConnected(url, 'GET');
	}

	async getPronosticByMatch(id){
		const url = `pronostic/getPronosticByMatch?matchId=${id}`;
		return await this.executeConnected(url,'GET');
	}

	async getMatchById(id){
		const url = `match?matchId=${id}`;
		return await this.executeConnected(url, 'GET');
	}
	
}

export default new MatchService();