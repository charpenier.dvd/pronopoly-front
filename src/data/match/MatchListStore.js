import {
	ReduceStore
} from 'flux/utils';
import {
	MatchActionTypes
} from '../ActionTypes'
import PronopolyDispatcher from '../PronopolyDispatcher';

class MatchListStore extends ReduceStore {

	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			matchs: []
		}
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case MatchActionTypes.MATCH_GET:
				state.matchs = action.matchs.matchs;
				this.__emitChange();
				break;
			default:
				break;
		}
		return state;
	}
}

export default new MatchListStore();