import PronopolyDispatcher from '../PronopolyDispatcher';
import MatchAPI from './MatchAPI';
import {
	AuthenticationException
} from './../../exception/Exception';
import {
	MatchActionTypes,
	LoginActionTypes
} from './../ActionTypes';

const MatchActions = {
	async getMatchs() {
		try {
			const matchs = await MatchAPI.getNextMatchs();
			PronopolyDispatcher.dispatch({
				type: MatchActionTypes.MATCH_GET,
				matchs: matchs
			});
		} catch (err) {
			if (err instanceof AuthenticationException) {
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
		}
	},
	async getMatchDetails(id){
		try{
			let match, pronostics;
			[match, pronostics] = await Promise.all([MatchAPI.getMatchById(id), MatchAPI.getPronosticByMatch(id)]);
			PronopolyDispatcher.dispatch({
				type: MatchActionTypes.MATCH_DETAILS_GET,
				match: match.match,
				pronostics: pronostics.pronostics
			});
		} catch(err){
			if (err instanceof AuthenticationException) {
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
		}
	}
};

export default MatchActions;