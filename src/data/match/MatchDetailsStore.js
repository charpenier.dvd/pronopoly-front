import {
	ReduceStore
} from 'flux/utils';
import {
	MatchActionTypes
} from '../ActionTypes'
import PronopolyDispatcher from '../PronopolyDispatcher';

class MatchDetailsStore extends ReduceStore {

	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			match: '',
			pronostics: []
		}
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case MatchActionTypes.MATCH_DETAILS_GET:
				state.match = action.match;
				state.pronostics = action.pronostics;
				this.__emitChange();
				break;
			default:
				break;
		}
		return state;
	}
}

export default new MatchDetailsStore();