import React from 'react';
import MatchActions from './MatchActions';
import MatchListStore from './MatchListStore';
import FluxComponent from '../../helper/FluxComponent';
import { Link } from 'react-router-dom';
import { tradGroup,isTeamWinner } from '../../helper/ComponentHelper';

class MatchListComponent extends FluxComponent {
	constructor(props) {
		super(props, MatchListStore);
	}

	componentDidMount() {
		super.componentDidMount();
		MatchActions.getMatchs();
	}


	render() {
		const { matchs } = this.state;
		return (
			<div className="container">
				<h1 className="display-4">Les Matchs</h1>
				{matchs.map((match, index) => {
					const fontTeamWinner1 = isTeamWinner(match.match_type, match.team_1_score, match.team_2_score, match.team_1_score_penalty, match.team_2_score_penalty);
					const fontTeamWinner2 = isTeamWinner(match.match_type, match.team_2_score, match.team_1_score, match.team_2_score_penalty, match.team_1_score_penalty);
					return <div className={index % 2 === 0 ? "row bg-light align-items-center" : "row align-items-center"} key={match.id}>
						<div className="col-md-2 col-12">
							{new Date(match.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}
						</div>
						<div className="col-md-1 col-12">
							<small>{tradGroup(match.match_type, match.group_letter)}</small>
						</div>
						<div className="col-md-2 text-right d-none d-sm-block">
							<span className={fontTeamWinner1}>
								{match.team_1_name}
								<img src="/flag/blank.gif" className={"ml-2 flag flag-" + match.team_1_flag} alt={match.team_1_flag} />
							</span>
						</div>

						<div className="col-10 d-block d-sm-none">
							<span className={fontTeamWinner1}>
								<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_1_flag} alt={match.team_1_flag} />
								{match.team_1_name}
							</span>
						</div>
						<div className="col-2 d-block d-sm-none">
							<span className={fontTeamWinner1}>
								{match.team_1_score}
							</span>
							{match.match_type !== "Group" && match.team_2_score === match.team_1_score && match.team_1_score ?
								<span>(<span className={fontTeamWinner1}>{match.team_1_score_penalty}</span>)</span>
								: ""}
						</div>

						<div className="col-md-2 text-center d-none d-sm-block">
							<span className={fontTeamWinner1}>
								{match.team_1_score}
							</span>
							<span> - </span>
							<span className={fontTeamWinner2}>
								{match.team_2_score}
							</span>
							{match.match_type !== "Group" && match.team_2_score === match.team_1_score && match.team_1_score ?
								<span>(<span className={fontTeamWinner1}>{match.team_1_score_penalty}</span> - <span className={fontTeamWinner2}>{match.team_2_score_penalty}</span>)</span>
								: ""}
						</div>
						<div className="col-md-3 text-left d-none d-sm-block">
							<span className={fontTeamWinner2}>
								<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_2_flag} alt={match.team_2_flag} />
								{match.team_2_name}
							</span>
						</div>

						<div className="col-10 d-block d-sm-none">
							<span className={fontTeamWinner2}>
								<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_2_flag} alt={match.team_2_flag} />
								{match.team_2_name}
							</span>
						</div>
						<div className="col-2 d-block d-sm-none">
							<span className={fontTeamWinner2}>
								{match.team_2_score}
							</span>
							{match.match_type !== "Group" && match.team_2_score === match.team_1_score && match.team_1_score?
								<span>(<span className={fontTeamWinner2}>{match.team_2_score_penalty}</span>)</span>
								: ""}
						</div>

						<div className="col-md-2 text-right d-none d-sm-block">
							{new Date(match.date) < Date.now() ? <Link className="btn btn-outline-info btn-sm" to={{ pathname: '/match/details/' + match.id }}>Voir les pronostics</Link> : ''}
						</div>

						<div className="col-12 d-block d-sm-none">
							{new Date(match.date) < Date.now() ? <Link className="btn btn-outline-info btn-sm mb-2" to={{ pathname: '/match/details/' + match.id }}>Voir les pronostics</Link> : ''}
						</div>
					</div>
				})}
			</div >
		);
	}
}

export default MatchListComponent;