import ApiHelper from '../../helper/ApiHelper'

class PronosticService extends ApiHelper {


	async getNextMatchs(){
		const url = 'match/nextMatchs';
		return await this.executeConnected(url, 'GET');
	}

	async getCurrentUserPronostics(){
		const url = 'pronostic/getPronosticByCurrentUser';
		return await this.executeConnected(url, 'GET');
	}

	async postPronostic(pronostic){
		const url = 'pronostic/create';
		const param = JSON.stringify({
			scoreEquipe1 : pronostic.pronostic_team_1,
			scoreEquipe2 : pronostic.pronostic_team_2,
			matchId : pronostic.id,
			pronosticWinner : pronostic.pronostic_winner
		});
		return await this.executeConnected(url,'POST', param);
	}
	
}

export default new PronosticService();