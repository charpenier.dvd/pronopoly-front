import {
	ReduceStore
} from 'flux/utils';
import PronopolyDispatcher from '../PronopolyDispatcher';
import {
	LoginActionTypes
} from '../ActionTypes';

class PronosticStore extends ReduceStore {
	constructor(prono) {
		super(PronopolyDispatcher);
		prono.status = 'registered';
		this.prono = prono;
	}

	getProno() {
		return this.prono;
	}

	getInitialState() {
		return this.getProno;
	}

	reduce(state, action) {
		switch (action.type) {
			case `PRONOSTIC_UPDATE_${this.prono.id}`:
				state = this.prono;
				state[action.name] = action.value;
				if (action.name !== 'pronostic_winner') {
					state.pronostic_winner = null;
				}
				this.prono = state;
				state.status = 'updated';
				this.__emitChange();
				break;
			case `PRONOSTIC_PENDING_${this.prono.id}`:
				state = this.prono;
				state.status = 'pending';
				this.__emitChange();
				break;
			case `PRONOSTIC_CREATED_${this.prono.id}`:
				state = this.prono;
				state.status = 'registered';
				//this.prono = state;
				this.__emitChange();
				break;
			case `PRONOSTIC_ERROR_${this.prono.id}`:
				//state = this.prono;
				state.status = 'error';
				this.__emitChange();
				break;
			case LoginActionTypes.DISCONNECT:
				state = this.getInitialState();
				this.__emitChange();
				break;
			default:
				break;
		}
		return state;
	}
}

export default PronosticStore;