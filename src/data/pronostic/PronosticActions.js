import {
	PronosticActionTypes
} from '../ActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';
import PronosticAPI from './PronosticAPI';
import {
	AuthenticationException
} from './../../exception/Exception';
import LoginActionTypes from './../login/LoginActionTypes';

const PronosticsActions = {

	async getPronostics() {
		try {
			let nextMatchs, pronostics;
			[nextMatchs, pronostics] = await Promise.all([PronosticAPI.getNextMatchs(), PronosticAPI.getCurrentUserPronostics()]);
			let pronoStore = [];
			nextMatchs.matchs.forEach(match => {
				const pronoForMatch = pronostics.pronostics.find(x => x.match_id === match.id);
				match.pronostic_team_1 = '';
				match.pronostic_team_2 = '';
				match.pronostic_winner = '';
				match.groupLetter = match.group_letter;
				if (pronoForMatch) {
					match.pronostic_team_1 = pronoForMatch.pronostic_team_1_score;
					match.pronostic_team_2 = pronoForMatch.pronostic_team_2_score;
					match.pronostic_winner = pronoForMatch.pronostic_winner;
				}
				match.status = "Enregistré";
				match.statusClass ="success";
				pronoStore.push(match);				
			});

			PronopolyDispatcher.dispatch({
				type: PronosticActionTypes.PRONOSTIC_GET,
				pronostics: pronoStore
			});

		} catch (err) {
			if (err instanceof AuthenticationException) {
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
		}
	},
	handleInputChange(event, id) {
		const target = event.target;
		const value = target.value;
		const name = target.name;
		PronopolyDispatcher.dispatch({
			type: PronosticActionTypes.PRONOSTIC_UPDATE,
			name: name,
			value: value,
			index: id
		});
	},
	async pronostic(index, userPronostic) {
		PronopolyDispatcher.dispatch({
			type: PronosticActionTypes.PRONOSTIC_PENDING,
			index: index
		});

		try {
			await PronosticAPI.postPronostic(userPronostic);
			PronopolyDispatcher.dispatch({
				type: PronosticActionTypes.PRONOSTIC_CREATED,
				index: index
			});
		} catch (err) {
			if (err instanceof AuthenticationException) {
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
			PronopolyDispatcher.dispatch({
				type: PronosticActionTypes.PRONOSTIC_ERROR,
				index: index
			});
		}
	},
	async updateAllPronostics(pronostics){
		pronostics.forEach((prono, index) =>{
			if(prono.status === "Modifié"){
				this.pronostic(index, prono);
			}
		});
	}
};

export default PronosticsActions;