import React from 'react';
import PronosticActions from './PronosticActions';
import PronosticListStore from './PronosticListStore';
import FluxComponent from '../../helper/FluxComponent';
import { tradGroup } from '../../helper/ComponentHelper';

class PronosticListComponent extends FluxComponent {
	constructor(props) {
		super(props, PronosticListStore);
	}

	componentWillMount() {
		PronosticActions.getPronostics();
	}

	handleInputChange = (event, index) => {
		PronosticActions.handleInputChange(event, index);
	}

	handleSubmit = (event, index, userProno) => {
		event.preventDefault();
		PronosticActions.pronostic(index, userProno);
	}

	handleAllSubmit = (event, pronostics) => {
		event.preventDefault();
		PronosticActions.updateAllPronostics(pronostics);
	}

	render() {
		const { pronostics } = this.state;
		return (
			<div className="container">
				<h1 className="display-4">Mes pronostics</h1>
				<div className="row">
					<div className="col text-right">
						<button type="button" className="btn btn-primary mb-2" onClick={(e) => this.handleAllSubmit(e, pronostics)}>Enregistrer tout</button>
					</div>
				</div>
				{
					pronostics.map((item, index) =>
						<form onSubmit={(e) => this.handleSubmit(e, index, item)} key={index}>
							<div className={index % 2 === 0 ? "row bg-light  align-items-center" : "row  align-items-center"} >
								<div className="col-md-2 col-8">
									{new Date(item.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}
								</div>
								<div className="col-md-2 col-4">
									<small>{tradGroup(item.match_type, item.groupLetter)}</small>
								</div>
								<div className="col-md-2 text-right d-none d-sm-block">
									<span>
										{item.team_1_name}
										<img src="/flag/blank.gif" className={"ml-2 flag flag-" + item.team_1_flag} alt={item.team_1_name} />
									</span>
								</div>
								<div className="col-md-2 text-center d-none d-sm-block form-group mt-1 mb-1">
									<div className="row  align-items-center">
										<div className="col text-right">
											<input type="text" name="pronostic_team_1" required
												value={item.pronostic_team_1} className="form-control"
												onChange={(e) => this.handleInputChange(e, index)} />
										</div>
										<div className="col text-left">
											<input type="text" name="pronostic_team_2" required
												value={item.pronostic_team_2} className="form-control"
												onChange={(e) => this.handleInputChange(e, index)} />
										</div>
									</div>
								</div>

								<div className="col-9 d-block d-sm-none">
									<span>
										<img src="/flag/blank.gif" className={"mr-2 flag flag-" + item.team_1_flag} alt={item.team_1_name} />
										{item.team_1_name}
									</span>
								</div>
								<div className="col-3 d-block d-sm-none">
									<input type="text" name="pronostic_team_1"
										value={item.pronostic_team_1} className="form-control mb-2"
										onChange={(e) => this.handleInputChange(e, index)} />
								</div>

								<div className="col-9 d-block d-sm-none">
									<span>
										<img src="/flag/blank.gif" className={"mr-2 flag flag-" + item.team_2_flag} alt={item.team_2_name} />
										{item.team_2_name}
									</span>
								</div>
								<div className="col-3 d-block d-sm-none">
									<input type="text" name="pronostic_team_2"
										value={item.pronostic_team_2} className="form-control"
										onChange={(e) => this.handleInputChange(e, index)} />
								</div>

								<div className="col-md-2 text-left d-none d-sm-block">
									<span>
										<img src="/flag/blank.gif" className={"mr-2 flag flag-" + item.team_2_flag} alt={item.team_2_name} />
										{item.team_2_name}
									</span>
								</div>
								<div className="col-md-2 text-right d-none d-sm-block">
									<input type="submit" className={"btn btn-outline-" + item.statusClass} value={item.status} />
								</div>
							</div>
							{
								item.match_type !== "Group" &&
									parseInt(item.pronostic_team_1, 10) === parseInt(item.pronostic_team_2, 10) ?
									<div className={index % 2 === 0 ? "row bg-light  align-items-center" : "row  align-items-center"}>
										<div className="col-md-4 col-12">
											<h4>Séléctionnez un gagnant</h4>
										</div>
										<div className="col-md-2 col-12 custom-control custom-radio custom-control-inline ml-4">
											<input type="radio" value={item.team_1_id} id={"customRadioInlineTeam1_" + index}
												checked={parseInt(item.team_1_id, 10) === parseInt(item.pronostic_winner, 10)}
												name="pronostic_winner" className="custom-control-input" required
												onChange={(e) => this.handleInputChange(e, index)} />
											<label className="custom-control-label" htmlFor={"customRadioInlineTeam1_" + index}>{item.team_1_name}</label>
										</div>

										<div className="col-md-2 col-12 custom-control custom-radio custom-control-inline ml-4">

											<input type="radio" value={item.team_2_id} className="custom-control-input"
												id={"customRadioInlineTeam2_" + index} required
												checked={parseInt(item.team_2_id, 10) === parseInt(item.pronostic_winner, 10)}
												name="pronostic_winner" onChange={(e) => this.handleInputChange(e, index)} />
											<label className="custom-control-label" htmlFor={"customRadioInlineTeam2_" + index}>{item.team_2_name}</label>
										</div>
									</div> : ''
							}
							<div className={index % 2 === 0 ? "row text-center d-block d-sm-none bg-light" : "row text-center d-block d-sm-none"}>
								<div className="col">
									<input type="submit" className={"mb-2 mt-2 btn btn-sm btn-outline-" + item.statusClass} value={item.status} />
								</div>
							</div>
						</form>
					)
				}
				<div className="row">
					<div className="col text-right">
						<button type="button" className="btn btn-primary mb-2" onClick={(e) => this.handleAllSubmit(e, pronostics)}>Enregistrer tout</button>
					</div>
				</div>
			</div>
		);
	}
}

export default PronosticListComponent;