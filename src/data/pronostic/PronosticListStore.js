import {
	ReduceStore
} from 'flux/utils';
import {
	PronosticActionTypes,
	LoginActionTypes
} from '../ActionTypes';
import PronopolyDispatcher from '../PronopolyDispatcher';

class PronosticListStore extends ReduceStore {

	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			pronostics: []
		}
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case PronosticActionTypes.PRONOSTIC_GET:
				state.pronostics = action.pronostics;
				this.__emitChange();
				break;
			case LoginActionTypes.DISCONNECT:
				state = this.getInitialState();
				this.__emitChange();
				break
			case PronosticActionTypes.PRONOSTIC_UPDATE:
				state.pronostics[action.index][action.name] = action.value;
				if (action.name !== 'pronostic_winner') {
					state.pronostics[action.index].pronostic_winner = null;
				}
				state.pronostics[action.index].status = 'Modifié';
				state.pronostics[action.index].statusClass = 'warning';
				this.__emitChange();
				break;
			case PronosticActionTypes.PRONOSTIC_PENDING:
				state.pronostics[action.index].status = 'Modifié';
				state.pronostics[action.index].statusClass = 'warning';
				this.__emitChange();
				break;
			case PronosticActionTypes.PRONOSTIC_CREATED:
				state.pronostics[action.index].status = 'Enregistré';
				state.pronostics[action.index].statusClass = 'success';
				this.__emitChange();
				break;
			case PronosticActionTypes.PRONOSTIC_ERROR:
				state.pronostics[action.index].status = 'Erreur';
				state.pronostics[action.index].statusClass = 'danger';
				this.__emitChange();
				break;
			default:
				return state;
		}
		return state;
	}
}

export default new PronosticListStore();