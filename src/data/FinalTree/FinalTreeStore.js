import {
	ReduceStore
} from 'flux/utils';
import {
	FinalTreeActionTypes
} from '../ActionTypes'
import PronopolyDispatcher from '../PronopolyDispatcher';

class FinalTreeStore extends ReduceStore {

	constructor() {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			eigths : [],
			quarts : [],
			halfFinals :[],
			final: {},
			thirdPlace : {}
		}
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case FinalTreeActionTypes.FINAL_TREE_GET:
				state.eigths = action.eigths;
				state.quarts = action.quarts;
				state.halfFinals = action.halfFinals;
				state.final = action.final;
				state.thirdPlace = action.thirdPlace;
				this.__emitChange();
				break;
			default:
				break;
		}
		return state;
	}
}

export default new FinalTreeStore();