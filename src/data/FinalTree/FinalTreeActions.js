import PronopolyDispatcher from '../PronopolyDispatcher';
import MatchAPI from './../match/MatchAPI';
import {
	AuthenticationException
} from './../../exception/Exception';
import {
	FinalTreeActionTypes,
	LoginActionTypes
} from './../ActionTypes';

const FinalTreeActions = {
	async getMatchs() {
		try {
			let matchs = await MatchAPI.getNextMatchs();
			matchs.matchs.sort(function (a, b) {
				const aInt = parseInt(a.id, 10);
				const bInt = parseInt(b.id, 10);
				if(aInt > bInt){
					return 1;
				} 
				if(aInt < bInt){
					return -1;
				}
				return 0;
			});

			const eights = matchs.matchs.filter(match => match.match_type === "Round of 16");
			const quarts = matchs.matchs.filter(match => match.match_type === "Quarter-finals");
			const half = matchs.matchs.filter(match => match.match_type === "Semi-finals");
			const third = matchs.matchs.filter(match => match.match_type === "Third place");
			const final = matchs.matchs.find(match => match.match_type === "Final");

			PronopolyDispatcher.dispatch({
				type: FinalTreeActionTypes.FINAL_TREE_GET,
				eigths: eights,
				quarts: quarts,
				halfFinals: half,
				final: final,
				thirdPlace: third
			});
		} catch (err) {
			if (err instanceof AuthenticationException) {
				PronopolyDispatcher.dispatch({
					type: LoginActionTypes.DISCONNECT
				});
			} else {
				console.log(err);
			}
		}
	}
};

export default FinalTreeActions;