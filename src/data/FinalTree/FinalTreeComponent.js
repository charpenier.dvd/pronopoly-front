import React from 'react';
import FluxComponent from '../../helper/FluxComponent';
import FinalTreeStore from './FinalTreeStore';
import FinalTreeActions from './FinalTreeActions';
import { isTeamWinner } from '../../helper/ComponentHelper';

class FinalTreeComponent extends FluxComponent {
	constructor(props) {
		super(props, FinalTreeStore);
	}

	componentDidMount() {
		super.componentDidMount();
		FinalTreeActions.getMatchs();
	}


	render() {
		const isFinal = this.state.eigths.length > 0 ? true : false;
		const final = this.state.final;
		return (
			!isFinal ? "" :
				<div>
					<h2>Phase finale</h2>
					<hr />
					<div className="container">
						<div className="row justify-content-between">
							<div className="col-12 d-block d-sm-none">
								<h3>Huitièmes de Finale</h3>
							</div>
							<div className="col-md-3 col-12">
								{this.state.eigths.map(function (match, index) {
									const fontTeamWinner1 = isTeamWinner(match.match_type, match.team_1_score, match.team_2_score, match.team_1_score_penalty, match.team_2_score_penalty);
									const fontTeamWinner2 = isTeamWinner(match.match_type, match.team_2_score, match.team_1_score, match.team_2_score_penalty, match.team_1_score_penalty);
									return <div className="row border bg-light mb-2" key={index}>
										<div className="col-12">
											<small className="font-weight-light">Le {new Date(match.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}</small>
										</div>
										<div className={`${fontTeamWinner1} col-8`}>
											<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_1_flag} alt={match.team_1_flag} />
											{match.team_1_name}
										</div>
										<div className={`${fontTeamWinner1} col-4 text-right`}>
											{match.team_1_score} {match.team_1_score === match.team_2_score && match.team_1_score ? <span>({match.team_1_score_penalty})</span> : ""}
										</div>
										<div className={`${fontTeamWinner2} col-8`}>
											<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_2_flag} alt={match.team_2_flag} />
											{match.team_2_name}
										</div>
										<div className={`${fontTeamWinner2} col-4 text-right`}>
											{match.team_2_score} {match.team_1_score === match.team_2_score && match.team_1_score ? <span>({match.team_2_score_penalty})</span> : ""}
										</div>
									</div>
								})}
							</div>
							<div className="col-md-3 col-12">
								<div className="row align-items-center h-100">
									<div className="col-12 d-block d-sm-none">
										<h3>Quarts de Finale</h3>
									</div>
									{this.state.quarts.map(function (match, index) {
										const fontTeamWinner1 = isTeamWinner(match.match_type, match.team_1_score, match.team_2_score, match.team_1_score_penalty, match.team_2_score_penalty);
										const fontTeamWinner2 = isTeamWinner(match.match_type, match.team_2_score, match.team_1_score, match.team_2_score_penalty, match.team_1_score_penalty);
										return <div className="col-12" key={index}>
											<div className="row border bg-light mb-2 ml-md-1">
												<div className="col-12">
													<small className="font-weight-light">Le {new Date(match.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}</small>
												</div>
												<div className={`${fontTeamWinner1} col-8`}>
													<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_1_flag} alt={match.team_1_flag} />
													{match.team_1_name}
												</div>
												<div className={`${fontTeamWinner1} col-4 text-right`}>
													{match.team_1_score} {match.team_1_score === match.team_2_score && match.team_1_score ? <span>({match.team_1_score_penalty})</span> : ""}
												</div>
												<div className={`${fontTeamWinner2} col-8`}>
													<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_2_flag} alt={match.team_2_flag} />
													{match.team_2_name}
												</div>
												<div className={`${fontTeamWinner2} col-4 text-right`}>
													{match.team_2_score} {match.team_1_score === match.team_2_score && match.team_1_score ? <span>({match.team_2_score_penalty})</span> : ""}
												</div>
											</div>
										</div>
									})}
								</div>
							</div>
							<div className="col-md-3 col-12">
								<div className="row align-items-center h-100">
									<div className="col-12 d-block d-sm-none">
										<h3>Demi-finales</h3>
									</div>
									{this.state.halfFinals.map(function (match, index) {
										const fontTeamWinner1 = isTeamWinner(match.match_type, match.team_1_score, match.team_2_score, match.team_1_score_penalty, match.team_2_score_penalty);
										const fontTeamWinner2 = isTeamWinner(match.match_type, match.team_2_score, match.team_1_score, match.team_2_score_penalty, match.team_1_score_penalty);
										return <div className="col-12" key={index}>
											<div className="row border bg-light mb-2 ml-md-1">
												<div className="col-12">
													<small className="font-weight-light">Le {new Date(match.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}</small>
												</div>
												<div className={`${fontTeamWinner1} col-8`}>
													<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_1_flag} alt={match.team_1_flag} />
													{match.team_1_name}
												</div>
												<div className={`${fontTeamWinner1} col-4`}>
													{match.team_1_score} {match.team_1_score === match.team_2_score && match.team_1_score ? <span>({match.team_1_score_penalty})</span> : ""}
												</div>
												<div className={`${fontTeamWinner2} col-8`}>
													<img src="/flag/blank.gif" className={"mr-2 flag flag-" + match.team_2_flag} alt={match.team_2_flag} />
													{match.team_2_name}
												</div>
												<div className={`${fontTeamWinner2} col-4`}>
													{match.team_2_score} {match.team_1_score === match.team_2_score && match.team_1_score ? <span>({match.team_2_score_penalty})</span> : ""}
												</div>
											</div>
										</div>
									})}
								</div>
							</div>
							<div className="col-md-3 col-12 align-items-center d-flex">
								{final ? <div>
									<div className="row d-block d-sm-none">
										<div className="col-12">
											<h3>Finale</h3>
										</div>
									</div>
									<div className="row border bg-light mb-2 ml-md-1">
										<div className="col-12">
											<small className="font-weight-light">Le {new Date(final.date).toLocaleDateString('fr-FR', { hour: 'numeric', minute: 'numeric' })}</small>
										</div>
										<div className={`col-8`}>
											<img src="/flag/blank.gif" className={"mr-2 flag flag-" + final.team_1_flag} alt={final.team_1_flag} />
											{final.team_1_name}
										</div>
										<div className={`col-4`}>
											{final.team_1_score} {final.team_1_score === final.team_2_score && final.team_1_score ? <span>({final.team_1_score_penalty})</span> : ""}
										</div>
										<div className={`col-8`}>
											<img src="/flag/blank.gif" className={"mr-2 flag flag-" + final.team_2_flag} alt={final.team_2_flag} />
											{final.team_2_name}
										</div>
										<div className={`col-4`}>
											{final.team_2_score} {final.team_1_score === final.team_2_score && final.team_1_score ? <span>({final.team_2_score_penalty})</span> : ""}
										</div>
									</div></div> : ""}
							</div>
						</div>
					</div>
				</div>
		);
	}
}

export default FinalTreeComponent;