import ApiHelper from '../../helper/ApiHelper'

class ResetPasswordService extends ApiHelper {

	async resetPasswordRequest(url, email) {
		const urlPost = 'user/sendResetPassword';
		const param = JSON.stringify({
			url: url,
			email: email
		});
		return await this.execute(urlPost, 'POST', param);
	}

	async resetPassword(newPassword, token) {
		const url = 'user/resetPassword';
		const param = JSON.stringify({
			password: newPassword,
			token: token
		});
		return await this.execute(url, 'POST', param);
	}

}

export default new ResetPasswordService();