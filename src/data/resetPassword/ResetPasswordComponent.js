import React from 'react';
import ResetPasswordActions from './ResetPasswordActions';
import ResetPasswordStore from './ResetPasswordStore';
import FluxComponent from '../../helper/FluxComponent';
import { Link } from 'react-router-dom';

class ResetPasswordComponent extends FluxComponent {
	constructor(props) {
		super(props, ResetPasswordStore);
	}
	handleInputChange(event) {
		ResetPasswordActions.handleInputChange(event);
	}

	handleSubmit = (event) => {
		event.preventDefault();

		let isValid = true;

		const forms = document.getElementsByClassName('needs-validation');
		Array.from(forms).forEach(function (form) {
			if (form.checkValidity() === false) {
				isValid = false;
			}
			form.classList.add('was-validated');
		});

		if (isValid) {
			ResetPasswordActions.resetRequest(this.state.email, this.state.url);
		}
	}


	render() {
		return (
			<div className="container">
				<div className="row justify-content-around mt-4 bg-opacity">
					<div className="col-md-4">
						<div className="card">
							<div className="card-body">
								<h2>Changement de mot de passe</h2>
								<form onSubmit={this.handleSubmit} className="needs-validation" noValidate>
									<div className="form-group">
										<label htmlFor="email">Email</label>
										<input type="email" name="email" id="email" className="form-control"
											value={this.state.email} onChange={this.handleInputChange} required />
										<div className="invalid-feedback">
											Veuillez saisir votre email
        								</div>
									</div>
									<button type="submit" className="btn btn-primary mb-2">Valider</button>
									<Link to='/' className="btn btn-secondary ml-2 mb-2" role="button">Annuler</Link>
									{this.state.status ?
										<div className="alert alert-primary" role="alert">
											{this.state.status}
										</div>
										: ''
									}
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ResetPasswordComponent;