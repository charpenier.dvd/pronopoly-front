import {
	ReduceStore
} from 'flux/utils';
import PronopolyDispatcher from '../PronopolyDispatcher';
import {
	ResetPasswordActionTypes
} from '../ActionTypes';
import {
	domain
} from './../../helper/Config';

class ResetPasswordStore extends ReduceStore {
	constructor(prono) {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			email: "",
			url: `${domain}/resetPasswordForm`,
			status: ""
		};
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case ResetPasswordActionTypes.RESET_PASSWORD_REQUEST:
				state.status = action.message;
				this.__emitChange();
				break;
			case ResetPasswordActionTypes.RESET_PASSWORD_UPDATE:
				state[action.name] = action.value;
				this.__emitChange();
				break
			default:
				return state;
		}
		return state;
	}
}

export default new ResetPasswordStore();