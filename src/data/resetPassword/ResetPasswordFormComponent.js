import React from 'react';
import ResetPasswordFormActions from './ResetPasswordFormActions';
import ResetPasswordFormStore from './ResetPasswordFormStore';
import FluxComponent from '../../helper/FluxComponent';
import { Link } from 'react-router-dom';

class ResetPasswordFormComponent extends FluxComponent {
	constructor(props) {
		super(props, ResetPasswordFormStore);

		const params = this.props.location.search.split('?token=');
		this.token = null;
		if (params && params.length > 0) {
			this.token = params[1];
		}
	}
	handleInputChange(event) {
		ResetPasswordFormActions.handleInputChange(event);
	}

	handleSubmit = (event) => {
		event.preventDefault();

		let isValid = true;

		const forms = document.getElementsByClassName('needs-validation');
		Array.from(forms).forEach(function (form) {
			if (form.checkValidity() === false) {
				isValid = false;
			}
			form.classList.add('was-validated');
		});

		if (isValid) {
			ResetPasswordFormActions.resetPassword(this.state.password, this.token);
		}
	}


	render() {
		return (
			<div className="container">
				<div className="row justify-content-around mt-4 bg-opacity">
					<div className="col-md-4">
						<div className="card">
							<div className="card-body">
								<h2>Réinitialisation de mot de passe</h2>
								<form onSubmit={this.handleSubmit} className="needs-validation" noValidate>
									<div className="form-group">
										<label htmlFor="password">Mot de passe*</label>
										<input type="password" name="password" id="password"
											className="form-control" required
											value={this.state.password} onChange={this.handleInputChange} />
										<div className="invalid-feedback">
											Veuillez saisir votre mot de passe
									</div>
									</div>
									<div className="form-group">
										<label htmlFor="confirmPassword">Confirmer votre mot de passe*</label>
										<input type="password" name="confirmPassword"
											className="form-control" required
											id="confirmPassword" value={this.state.confirmPassword} onChange={this.handleInputChange} />
										<div className="invalid-feedback">
											Veuillez confirmer votre mot de passe
									</div>
									</div>
									<button type="submit" className="btn btn-primary mb-2">Valider</button>
									<Link to='/' className="btn btn-secondary ml-2 mb-2" role="button">Retour</Link>
									{this.state.status ?
										<div className="alert alert-primary" role="alert">
											{this.state.status}
										</div>
										: ''
									}
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ResetPasswordFormComponent;