import {
	ReduceStore
} from 'flux/utils';
import PronopolyDispatcher from '../PronopolyDispatcher';
import {ResetPasswordActionTypes} from '../ActionTypes';

class ResetPasswordFormStore extends ReduceStore {
	constructor(prono) {
		super(PronopolyDispatcher);
	}

	getInitialState() {
		const obj = {
			password: "",
			confirmPassword : "",
			status : ""
		};
		return obj;
	}

	reduce(state, action) {
		switch (action.type) {
			case ResetPasswordActionTypes.RESET_PASSWORD:
				state.status = action.message;
				this.__emitChange();
				break;
			case ResetPasswordActionTypes.RESET_PASSWORD_FORM_UPDATE:
				state[action.name] = action.value;
				this.__emitChange();
				break
			default:
				return state;
		}
		return state;
	}
}

export default new ResetPasswordFormStore();