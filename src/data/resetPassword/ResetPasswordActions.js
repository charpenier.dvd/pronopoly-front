import PronopolyDispatcher from '../PronopolyDispatcher';
import ResetPasswordAPI from './ResetPasswordAPI';
import {
	ResetPasswordActionTypes
} from './../ActionTypes';


const ResetPasswordActions = {

	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		PronopolyDispatcher.dispatch({
			type: ResetPasswordActionTypes.RESET_PASSWORD_UPDATE,
			name: name,
			value: value
		});
	},
	async resetRequest(email, url){
		await ResetPasswordAPI.resetPasswordRequest(url, email);

		PronopolyDispatcher.dispatch({
			type: ResetPasswordActionTypes.RESET_PASSWORD_REQUEST,
			message: "Pour changer votre mot de passe, cliquer sur le lien présent dans l'email envoyé."
		});
	}
};

export default ResetPasswordActions;