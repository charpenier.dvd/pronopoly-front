import PronopolyDispatcher from '../PronopolyDispatcher';
import ResetPasswordAPI from './ResetPasswordAPI';
import {
	ResetPasswordActionTypes
} from './../ActionTypes';


const ResetPasswordFormActions = {

	handleInputChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		if (name === 'password' || name === 'confirmPassword') {
			const password = document.getElementById('password');
			const confifmPassword = document.getElementById('confirmPassword');
			password.setCustomValidity('');
			confifmPassword.setCustomValidity('');
			
			if (password.value !== confifmPassword.value) {
				password.setCustomValidity('invalid');
				confifmPassword.setCustomValidity('invalid');
			}
		}

		PronopolyDispatcher.dispatch({
			type: ResetPasswordActionTypes.RESET_PASSWORD_FORM_UPDATE,
			name: name,
			value: value
		});
	},
	async resetPassword(password, token){
		try{
			await ResetPasswordAPI.resetPassword(password, token);
			PronopolyDispatcher.dispatch({
				type: ResetPasswordActionTypes.RESET_PASSWORD,
				message: "Votre mot de passe est à jour"
			});
		} catch(ex){
			PronopolyDispatcher.dispatch({
				type: ResetPasswordActionTypes.RESET_PASSWORD,
				message: "Impossible de changer votre mot de passe"
			});
		}
	}
};

export default ResetPasswordFormActions;